//! This crate contains code used in both the backend as well as the front end.
//! Most notably it contains the format for the messages send between the frontend and the backend.
//! These messages are stored in there own module formated for the state they belong to
//! For example the messages which the client can send for the state Start is in
//! `msg::start::Client`
#[macro_use]
extern crate serde_derive;

pub mod msg;

/// A common trait which should be frontends implement.
pub trait Frontend {
    fn try_recv(&mut self) -> Option<msg::Client>;

    fn recv(&mut self) -> msg::Client;

    /// TODO add error handleing
    fn send(&mut self, msg: msg::Server);
}
