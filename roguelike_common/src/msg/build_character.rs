use immers::Patchable;

#[derive(Clone, Copy, Debug, Serialize, Deserialize, Eq, PartialEq)]
/// A stat
pub enum Stat {
    Strength,
    Agility,
    Intelligence,
    Constitution,
    Wisdom,
    Charisma,
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize, Patchable)]
/// A stat
#[patchable(derive(Copy, Clone, Debug, Serialize, Deserialize))]
pub struct Stats {
    pub strength: f32,
    pub agility: f32,
    pub intelligence: f32,
    pub constitution: f32,
    pub wisdom: f32,
    pub charisma: f32,
}

impl Default for Stats {
    fn default() -> Self {
        Stats {
            strength: 0.0,
            agility: 0.0,
            intelligence: 0.0,
            constitution: 0.0,
            wisdom: 0.0,
            charisma: 0.0,
        }
    }
}

impl Stats {
    pub fn get_stat(&self, stat: Stat) -> f32 {
        match stat {
            Stat::Strength => self.strength,
            Stat::Agility => self.agility,
            Stat::Intelligence => self.intelligence,
            Stat::Constitution => self.constitution,
            Stat::Wisdom => self.wisdom,
            Stat::Charisma => self.charisma,
        }
    }

    pub fn get_stat_mut(&mut self, stat: Stat) -> &mut f32 {
        match stat {
            Stat::Strength => &mut self.strength,
            Stat::Agility => &mut self.agility,
            Stat::Intelligence => &mut self.intelligence,
            Stat::Constitution => &mut self.constitution,
            Stat::Wisdom => &mut self.wisdom,
            Stat::Charisma => &mut self.charisma,
        }
    }
}

#[derive(Clone, Debug, Default, Serialize, Deserialize, Patchable)]
#[patchable(derive(Clone, Debug, Serialize, Deserialize))]
pub struct Character {
    pub first_name: String,
    pub last_name: String,
    pub stats: Stats,
    pub is_male: bool,
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize, Eq, PartialEq)]
pub enum Sex {
    Male,
    Female,
}

impl Default for Sex {
    fn default() -> Sex {
        Sex::Female
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum Server {
    /// Confirming a change to the character.
    Patch(<Character as Patchable>::Patch),
    /// Confirming stop building a character and go back to the previous page.
    Cancel,
    /// Confirming that the character building has finished and the state should transition
    Confirm,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum Client {
    /// A change to the character
    Patch(<Character as Patchable>::Patch),
    /// Stop building a character and go back to the previous page.
    Cancel,
    /// Message to signal that the character is ready.
    Confirm,
}
