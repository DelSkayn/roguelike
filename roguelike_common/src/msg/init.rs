//! Messages for reinitializing state

/// A message which is send when the frontend needs to be reinitialized
/// Each message corresponds to a state to add to the stack.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Server {
    /// The start state
    Start,
    /// Build character state
    BuildCharacter,
    /// The main game state,
    Main,
    /// Send after backend send all the messages neccesary for reinitializing the state.
    Done,
}
