//! All the messages send between client and server

pub mod build_character;
pub mod init;
pub mod main;
pub mod start;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Client {
    /// Send when frontend initialized
    Init,
    /// The start state
    Start(start::Client),
    /// The build character state
    BuildCharacter(build_character::Client),
    Main(main::Client),
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Server {
    /// Send to reinitialize state
    /// Is message for the frontend to add the corresponding state to the stack
    Init(init::Server),
    /// Send after frontend initialized meaning that the front end
    /// should start on start menu.
    Start(start::Server),
    /// The build character state
    BuildCharacter(build_character::Server),
    Main(main::Server),
}

impl Client {
    pub fn is_init(&self) -> bool {
        match *self {
            Client::Init => true,
            _ => false,
        }
    }
}

impl Server {
    pub fn is_init(&self) -> bool {
        match *self {
            Server::Init(_) => true,
            _ => false,
        }
    }
}
