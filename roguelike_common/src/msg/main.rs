use immers::Patchable;

#[derive(Debug, Clone, Default, Patchable)]
#[patchable(derive(Debug, Clone, Serialize, Deserialize))]
pub struct PlayerInfo {
    pub first_name: String,
    pub last_name: String,
}

#[derive(Clone, Debug, Serialize, Deserialize, Default)]
pub struct StoryState {
    pub text: String,
    pub options: Vec<String>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum Server {
    /// Change the state
    StateChange(StoryState),
    /// A change happend in player info
    PlayerInfo(<PlayerInfo as Patchable>::Patch),
    /// Exit the main game
    Exit,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum Client {
    TakeOption(usize),
    Exit,
}
