//! Messages for the start state

/// The messages send from the client in the start state;
#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Client {
    /// Send when a new game should be started
    NewGame,
    LoadGame,
    Quit,
}

/// The messages send from the Server in the start state;
#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Server {
    /// Send after init request when backend is in Server state.
    Init,
    /// Send when the backend allows transition to new game.
    NewGame,
}
