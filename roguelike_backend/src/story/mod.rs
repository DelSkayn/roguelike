mod builder;
pub use builder::{Config, StoryBuilder};

#[derive(Debug)]
pub enum TransitionType {
    Push(usize),
    Replace(usize),
    Pop,
}

#[derive(Debug)]
pub struct State {
    pub(crate) text: String,
}

#[derive(Debug)]
pub struct Transition {
    pub(crate) name: String,
    pub(crate) ty: TransitionType,
}

#[derive(Debug)]
pub struct Story {
    pub(crate) stack: Vec<usize>,
    pub(crate) states: Vec<State>,
    pub(crate) transitions: Vec<Vec<Transition>>,
}

impl Story {
    pub fn get_text(&self) -> String {
        let cur = *self.stack.last().unwrap();
        self.states[cur].text.clone()
    }

    pub fn get_options(&self) -> Vec<String> {
        let cur = *self.stack.last().unwrap();
        self.transitions[cur]
            .iter()
            .map(|x| x.name.clone())
            .collect()
    }

    pub fn take_option(&mut self, option: usize) {
        let cur = *self.stack.last().unwrap();
        match self.transitions[cur][option].ty {
            TransitionType::Pop => {
                if self.stack.len() == 1 {
                    error!("tried to pop last state!");
                    return;
                }
                self.stack.pop();
            }
            TransitionType::Push(x) => {
                self.stack.push(x);
            }
            TransitionType::Replace(x) => *self.stack.last_mut().unwrap() = x,
        }
    }
}
