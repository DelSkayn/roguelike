use super::Story;
use quick_error::quick_error;
use serde::{Deserialize, Serialize};
use std::{
    collections::hash_map::{Entry, HashMap},
    fs::File,
    io::{self, Read},
    mem,
    path::Path,
};

quick_error! {
    #[derive(Debug)]
    pub enum Error{
        Json(e: serde_json::Error){
            from()
            cause(e)
            display("error in json format: {}",e)
        }
        IO(e: io::Error){
            from()
            cause(e)
            display("io error: {}",e)
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Config {
    pub story_dir: String,
    pub start_state: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(rename_all = "snake_case")]
pub enum TransitionType {
    Push(String),
    Replace(String),
    Pop,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum Text {
    Inline(String),
    File { path: String },
}

impl Text {
    /// Make sure that the text is not a reference to a path but to a file.
    pub fn load_inline(&mut self, path_prefix: &str) -> Result<(), Error> {
        if let Text::File { path } = self {
            let path = Path::new(path_prefix).join(path);
            trace!("loading text form file: {}", path.to_str().unwrap());
            let mut file = File::open(path)?;
            let mut text = String::new();
            file.read_to_string(&mut text)?;
            *self = Text::Inline(text);
        }
        Ok(())
    }

    pub fn unwrap(self) -> String {
        match self {
            Text::Inline(x) => x,
            Text::File { path: _ } => {
                panic!("called unwrap on text which was not of variant Inline")
            }
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Transition {
    pub name: String,
    pub ty: TransitionType,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct State {
    pub text: Text,
    #[serde(default)]
    pub transitions: Vec<Transition>,
}

pub struct StoryBuilder {
    initial: State,
    initial_name: String,
    states: HashMap<String, State>,
}

impl StoryBuilder {
    pub fn from_config(config: Config) -> Result<StoryBuilder, Error> {
        if cfg!(debug_assertions) && config.start_state.to_lowercase() != config.start_state {
            warn!("state name was not in snake_case, please only use lowercase for file names");
        }

        let initial_path = Path::new(&config.story_dir).join(config.start_state.clone() + ".json");
        // setup
        trace!(
            "loading story with start: {}",
            initial_path.to_str().unwrap()
        );
        let file = File::open(initial_path)?;
        let mut initial_state: State = serde_json::from_reader(file)?;
        initial_state.text.load_inline(&config.story_dir)?;
        let mut states = HashMap::new();
        states.insert(config.start_state.clone(), initial_state.clone());
        let mut pending = vec![initial_state.clone()];

        // loading all referenced stories
        while pending.len() > 0 {
            let cur_state = pending.pop().unwrap();
            for trans in cur_state.transitions {
                let name = match trans.ty {
                    TransitionType::Pop => continue,
                    TransitionType::Replace(ref x) => x,
                    TransitionType::Push(ref x) => x,
                };
                if cfg!(debug_assertions) && name.to_lowercase() != *name {
                    warn!(
                        "state name was not complete lowercase, please only use lowercase names!"
                    );
                }
                match states.entry(name.clone()) {
                    // Already loaded
                    Entry::Occupied(_) => {}
                    // Missing
                    Entry::Vacant(x) => {
                        let path = Path::new(&config.story_dir).join(name.to_string() + ".json");
                        trace!("loading state: {}", path.to_str().unwrap());
                        let file = File::open(path)?;
                        let mut state: State = serde_json::from_reader(file)?;
                        state.text.load_inline(&config.story_dir)?;
                        pending.push(x.insert(state).clone())
                    }
                }
            }
        }
        Ok(StoryBuilder {
            initial_name: config.start_state,
            initial: initial_state,
            states,
        })
    }

    fn build_transitions(
        next_idx: &mut usize,
        idx_assigner: &mut HashMap<String, usize>,
        mut transitions: Vec<Transition>,
    ) -> Vec<super::Transition> {
        transitions
            .drain(..)
            .map(|e| {
                let ty = match e.ty {
                    TransitionType::Push(x) => {
                        let idx = *idx_assigner.entry(x).or_insert_with(|| {
                            let res = *next_idx;
                            *next_idx += 1;
                            res
                        });
                        super::TransitionType::Push(idx)
                    }
                    TransitionType::Replace(x) => {
                        let idx = *idx_assigner.entry(x).or_insert_with(|| {
                            let res = *next_idx;
                            *next_idx += 1;
                            res
                        });
                        super::TransitionType::Replace(idx)
                    }
                    TransitionType::Pop => super::TransitionType::Pop,
                };

                super::Transition { ty, name: e.name }
            })
            .collect()
    }

    pub fn build(mut self) -> Story {
        let state = self.states.remove(&self.initial_name).unwrap();
        let mut states = vec![Some(super::State {
            text: state.text.unwrap(),
        })];
        let mut idx_assigner = HashMap::new();
        idx_assigner.insert(self.initial_name, 0);
        let mut next_idx = 1;
        let mut transitions = vec![Some(Self::build_transitions(
            &mut next_idx,
            &mut idx_assigner,
            state.transitions,
        ))];

        for (key, value) in self.states.drain() {
            let text = value.text;
            let state = super::State {
                text: text.unwrap(),
            };

            let transition =
                Self::build_transitions(&mut next_idx, &mut idx_assigner, value.transitions);
            let idx = *idx_assigner.entry(key).or_insert_with(|| {
                let res = next_idx;
                next_idx += 1;
                res
            });

            for _ in states.len()..idx + 1 {
                states.push(None);
            }
            for _ in transitions.len()..idx + 1 {
                transitions.push(None);
            }

            states[idx] = Some(state);
            transitions[idx] = Some(transition);
        }

        let states = states.into_iter().map(|e| e.unwrap()).collect();
        let transitions = transitions.into_iter().map(|e| e.unwrap()).collect();

        super::Story {
            stack: vec![0],
            states,
            transitions,
        }
    }
}
