use serde::de::DeserializeOwned;
use serde_json;
use std::fs::File;
use std::path::Path;

pub trait Config {
    fn load<P: AsRef<Path>>(path: P) -> Self;
}

impl<T: DeserializeOwned + Default> Config for T {
    fn load<P: AsRef<Path>>(path: P) -> Self {
        File::open(path)
            .map_err(|e| {
                error!("error loading config: {}", e);
                ()
            })
            .and_then(|x| {
                serde_json::from_reader(x).map_err(|e| {
                    error!("error loading config: {}", e);
                    ()
                })
            })
            .unwrap_or_default()
    }
}
