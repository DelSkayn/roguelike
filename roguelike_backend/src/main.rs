#![allow(dead_code)]
#![allow(unused_imports)]

#[macro_use]
extern crate log;
#[macro_use]
extern crate serde_derive;

use roguelike_common::{msg, Frontend};
use roguelike_server::{WebFrontend, WebFrontendConfig};

use crossterm::{input, Color, InputEvent, KeyEvent};
use specs::prelude::*;
use spin_sleep::LoopHelper;
use std::io::{self};

mod app;
mod config;
mod data_builder;
mod entity;
mod logger;
mod states;
mod story;
mod vector;

use app::{Application, Ctx, State, Transition};
use config::Config;
use data_builder::DataBuilder;
use logger::{LogConfig, Logger};
use states::StartState;
pub use vector::{IVector, Vector};

fn main() {
    let screen_logger_config = LogConfig::load("res/log_config.json");
    Logger::init(screen_logger_config).unwrap();

    let front_config = WebFrontendConfig::load("res/front_config.json");
    let front = WebFrontend::new(front_config);

    let story_config = story::Config {
        story_dir: "res/story".to_string(),
        start_state: "main".to_string(),
    };
    let builder = story::StoryBuilder::from_config(story_config).unwrap();
    let story = builder.build();

    let builder = DataBuilder::new().insert(story);
    /*
    .with(PlayerControllerSystem, "player_controller_system", &[])
    .with(
        PositionUpdateSystem::default(),
        "map_position_update",
        &["player_controller_system"],
    )
    .with(
        CameraController,
        "camera_controller",
        &["player_controller_system"],
    )
    .with(DeathSystem, "death", &["player_controller_system"]);
    */

    info!("finished initalizing engine");
    Application::new(StartState, front, builder, 60.0).run();
    info!("quiting, have a good day.");
}
