//! Application state machine utility.

use roguelike_common::{msg, Frontend};
use specs::{Dispatcher, DispatcherBuilder, World};
use spin_sleep::LoopHelper;

use crate::data_builder::DataBuilder;

pub mod prelude {
    pub use crate::app::{Ctx, State, Transition};
    pub use roguelike_common::{msg, Frontend};
    pub use specs::World;
}

/// The current frames per second
/// Updated periodicly. TODO: Fix spelling.
#[derive(Debug, Clone, Copy)]
pub struct Fps(f32);

/// A enum representing a anction the application state machine must take.
pub enum Transition<F> {
    /// Push a new state onto the stack
    Push(Box<dyn State<F>>),
    /// Pop the current state on the stack and go back to the previous state on the stack
    Pop,
    /// Continue running, recieving messages when they are available and ticking when ready
    Continue,
    /// Stop recieving ticks and wait for a message from the frontend.
    Wait,
    /// Replace the current state with the new state.
    Replace(Box<dyn State<F>>),
    /// Quit the main loop
    Quit,
}

impl<F> Transition<F> {
    fn is_continue(&self) -> bool {
        match *self {
            Transition::Continue => true,
            _ => false,
        }
    }
}

/// The context,
/// Currently used to send messages to the frontend
/// Might be used for more at a later date.
pub struct Ctx<'a, F: Frontend>(&'a mut F);

impl<F: Frontend> Ctx<'_, F> {
    pub fn send(&mut self, msg: msg::Server) {
        trace!("MESSAGE SEND: {:#?}", msg);
        self.0.send(msg)
    }
}

/// Trait for the state of the application
pub trait State<F: Frontend> {
    fn on_start(&mut self, _world: &mut World, _ctx: &mut Ctx<F>) -> Transition<F> {
        Transition::Continue
    }

    fn on_message(
        &mut self,
        _message: msg::Client,
        _world: &mut World,
        _ctx: &mut Ctx<F>,
    ) -> Transition<F> {
        Transition::Continue
    }

    fn tick(&mut self, _world: &mut World, _ctx: &mut Ctx<F>) -> Transition<F> {
        Transition::Continue
    }

    /// Called whenever the init message is recieved from the front end.
    /// Meaning that the frontend (re)initialized and needs to know what state it should be in.
    /// This function is called on the entire stack of states, from lowest to highest.
    fn init(&self, world: &mut World, ctx: &mut Ctx<F>);
}

/// The application state machine.
pub struct Application<'a, 'b, F: Frontend> {
    states: Vec<Box<dyn State<F>>>,
    dispatch: Dispatcher<'a, 'b>,
    world: World,
    front: F,
    loop_helper: LoopHelper,
}

enum LoopState {
    /// A new state has been push onto the state run the start function
    Start,
    /// Wait for a message to be recieved from the frontend
    Wait,
    /// Check for pending messages
    Pending,
    /// Run the world for a tick
    Tick,
}

impl<'a, 'b, F: Frontend> Application<'a, 'b, F> {
    /// Create a new application with a certain frontend, data, end frames per second
    pub fn new<S: State<F> + 'static>(
        intial_state: S,
        frontend: F,
        data: DataBuilder<'a, 'b>,
        fps: f32,
    ) -> Self {
        let mut world = data.world;
        let mut dispatch = data.dispatch.build();
        let loop_helper = LoopHelper::builder()
            .report_interval_s(0.5)
            .build_with_target_rate(fps);
        dispatch.setup(&mut world);
        Application {
            states: vec![Box::new(intial_state)],
            dispatch,
            world,
            front: frontend,
            loop_helper,
        }
    }

    fn handle_init(states: &Vec<Box<dyn State<F>>>, world: &mut World, ctx: &mut Ctx<F>) {
        for state in states.iter() {
            state.init(world, ctx);
        }
        ctx.send(msg::Server::Init(msg::init::Server::Done));
    }

    /// Run the application till it finishes
    pub fn run(mut self) {
        let mut state = LoopState::Start;
        self.loop_helper.loop_start();
        loop {
            let mut ctx = Ctx(&mut self.front);
            let cur_state = self.states.last_mut().unwrap();
            let trans = match state {
                LoopState::Start => cur_state.on_start(&mut self.world, &mut ctx),
                LoopState::Wait => {
                    let message = ctx.0.recv();
                    // Handeling the init message should be opague to the state.
                    // After handling init the state should continue as normal
                    trace!("MESSAGE RECV: {:#?}", message);
                    if message.is_init() {
                        Self::handle_init(&self.states, &mut self.world, &mut ctx);
                        continue;
                    }
                    cur_state.on_message(message, &mut self.world, &mut ctx)
                }
                LoopState::Pending => {
                    if let Some(x) = ctx.0.try_recv() {
                        trace!("MESSAGE RECV: {:#?}", x);
                        // Handeling the init message should be opague to the state.
                        // After handling init the state should continue as normal
                        if x.is_init() {
                            Self::handle_init(&self.states, &mut self.world, &mut ctx);
                            continue;
                        }
                        let inner_trans = cur_state.on_message(x, &mut self.world, &mut ctx);
                        if inner_trans.is_continue() {
                            continue;
                        } else {
                            inner_trans
                        }
                    } else {
                        state = LoopState::Tick;
                        continue;
                    }
                }
                LoopState::Tick => {
                    let trans = cur_state.tick(&mut self.world, &mut ctx);
                    self.loop_helper.loop_sleep();
                    self.loop_helper.loop_start();
                    trans
                }
            };
            match trans {
                Transition::Pop => {
                    self.states.pop();
                    if self.states.len() == 0 {
                        return;
                    }
                    state = LoopState::Start;
                }
                Transition::Push(x) => {
                    self.states.push(x);
                    state = LoopState::Start;
                }
                Transition::Quit => {
                    return;
                }
                Transition::Replace(x) => {
                    self.states.pop();
                    self.states.push(x);
                    state = LoopState::Start;
                }
                Transition::Wait => {
                    state = LoopState::Wait;
                }
                Transition::Continue => {
                    state = match state {
                        LoopState::Tick => LoopState::Pending,
                        LoopState::Pending => LoopState::Tick,
                        LoopState::Wait => LoopState::Tick,
                        LoopState::Start => LoopState::Pending,
                    };
                }
            }
        } // End loop
    }
}
