use std::{fmt, ops};

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Vector {
    pub x: f32,
    pub y: f32,
}

impl fmt::Display for Vector {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "[{},{}]", self.x, self.y)
    }
}

impl Vector {
    pub fn new(x: f32, y: f32) -> Self {
        Vector { x, y }
    }

    pub fn abs(mut self) -> Self {
        self.x = self.x.abs();
        self.y = self.y.abs();
        self
    }

    pub fn magnitude(self) -> f32 {
        (self.x * self.x + self.y + self.y).sqrt()
    }

    pub fn normalize(mut self) -> Self {
        self /= self.magnitude();
        self
    }

    pub fn round(mut self) -> Self {
        self.x = self.x.round();
        self.y = self.y.round();
        self
    }
}

impl ops::Neg for Vector {
    type Output = Vector;

    fn neg(mut self) -> Self {
        self.x = -self.x;
        self.y = -self.y;
        self
    }
}

impl ops::Div<f32> for Vector {
    type Output = Vector;

    fn div(mut self, rhs: f32) -> Self {
        self.x /= rhs;
        self.y /= rhs;
        self
    }
}

impl ops::DivAssign<f32> for Vector {
    fn div_assign(&mut self, rhs: f32) {
        self.x /= rhs;
        self.y /= rhs;
    }
}

impl ops::Div<Vector> for Vector {
    type Output = Vector;

    fn div(mut self, rhs: Self) -> Self {
        self.x /= rhs.x;
        self.y /= rhs.y;
        self
    }
}

impl ops::DivAssign<Vector> for Vector {
    fn div_assign(&mut self, rhs: Self) {
        self.x /= rhs.x;
        self.y /= rhs.y;
    }
}

impl ops::Mul<f32> for Vector {
    type Output = Vector;

    fn mul(mut self, rhs: f32) -> Self {
        self.x *= rhs;
        self.y *= rhs;
        self
    }
}

impl ops::MulAssign<f32> for Vector {
    fn mul_assign(&mut self, rhs: f32) {
        self.x *= rhs;
        self.y *= rhs;
    }
}

impl ops::Mul<Vector> for Vector {
    type Output = Vector;

    fn mul(mut self, rhs: Vector) -> Self {
        self.x *= rhs.x;
        self.y *= rhs.y;
        self
    }
}

impl ops::MulAssign<Vector> for Vector {
    fn mul_assign(&mut self, rhs: Vector) {
        self.x *= rhs.x;
        self.y *= rhs.y;
    }
}

impl ops::Add<f32> for Vector {
    type Output = Vector;

    fn add(mut self, rhs: f32) -> Self {
        self.x += rhs;
        self.y += rhs;
        self
    }
}

impl ops::AddAssign<f32> for Vector {
    fn add_assign(&mut self, rhs: f32) {
        self.x += rhs;
        self.y += rhs;
    }
}

impl ops::Add<Vector> for Vector {
    type Output = Vector;

    fn add(mut self, rhs: Vector) -> Self {
        self.x += rhs.x;
        self.y += rhs.y;
        self
    }
}

impl ops::AddAssign<Vector> for Vector {
    fn add_assign(&mut self, rhs: Vector) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl ops::Sub<f32> for Vector {
    type Output = Vector;

    fn sub(mut self, rhs: f32) -> Self {
        self.x -= rhs;
        self.y -= rhs;
        self
    }
}

impl ops::SubAssign<f32> for Vector {
    fn sub_assign(&mut self, rhs: f32) {
        self.x -= rhs;
        self.y -= rhs;
    }
}

impl ops::Sub<Vector> for Vector {
    type Output = Vector;

    fn sub(mut self, rhs: Vector) -> Self {
        self.x -= rhs.x;
        self.y -= rhs.y;
        self
    }
}

impl ops::SubAssign<Vector> for Vector {
    fn sub_assign(&mut self, rhs: Vector) {
        self.x -= rhs.x;
        self.y -= rhs.y;
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Hash, Eq)]
pub struct IVector {
    pub x: i32,
    pub y: i32,
}

impl IVector {
    pub fn new(x: i32, y: i32) -> Self {
        IVector { x, y }
    }

    pub fn abs(mut self) -> Self {
        self.x = self.x.abs();
        self.y = self.y.abs();
        self
    }

    pub fn div_euclid(mut self, rhs: i32) -> Self {
        self.x = self.x.div_euclid(rhs);
        self.y = self.y.div_euclid(rhs);
        self
    }

    pub fn rem_euclid(mut self, rhs: i32) -> Self {
        self.x = self.x.rem_euclid(rhs);
        self.y = self.y.rem_euclid(rhs);
        self
    }
}

impl fmt::Display for IVector {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "[{},{}]", self.x, self.y)
    }
}

impl ops::Neg for IVector {
    type Output = IVector;

    fn neg(mut self) -> Self {
        self.x = -self.x;
        self.y = -self.y;
        self
    }
}

impl ops::Div<i32> for IVector {
    type Output = IVector;

    fn div(mut self, rhs: i32) -> Self {
        self.x /= rhs;
        self.y /= rhs;
        self
    }
}

impl ops::DivAssign<i32> for IVector {
    fn div_assign(&mut self, rhs: i32) {
        self.x /= rhs;
        self.y /= rhs;
    }
}

impl ops::Div<IVector> for IVector {
    type Output = IVector;

    fn div(mut self, rhs: Self) -> Self {
        self.x /= rhs.x;
        self.y /= rhs.y;
        self
    }
}

impl ops::DivAssign<IVector> for IVector {
    fn div_assign(&mut self, rhs: Self) {
        self.x /= rhs.x;
        self.y /= rhs.y;
    }
}

impl ops::Mul<i32> for IVector {
    type Output = IVector;

    fn mul(mut self, rhs: i32) -> Self {
        self.x *= rhs;
        self.y *= rhs;
        self
    }
}

impl ops::MulAssign<i32> for IVector {
    fn mul_assign(&mut self, rhs: i32) {
        self.x *= rhs;
        self.y *= rhs;
    }
}

impl ops::Mul<IVector> for IVector {
    type Output = IVector;

    fn mul(mut self, rhs: IVector) -> Self {
        self.x *= rhs.x;
        self.y *= rhs.y;
        self
    }
}

impl ops::MulAssign<IVector> for IVector {
    fn mul_assign(&mut self, rhs: IVector) {
        self.x *= rhs.x;
        self.y *= rhs.y;
    }
}

impl ops::Add<i32> for IVector {
    type Output = IVector;

    fn add(mut self, rhs: i32) -> Self {
        self.x += rhs;
        self.y += rhs;
        self
    }
}

impl ops::AddAssign<i32> for IVector {
    fn add_assign(&mut self, rhs: i32) {
        self.x += rhs;
        self.y += rhs;
    }
}

impl ops::Add<IVector> for IVector {
    type Output = IVector;

    fn add(mut self, rhs: IVector) -> Self {
        self.x += rhs.x;
        self.y += rhs.y;
        self
    }
}

impl ops::AddAssign<IVector> for IVector {
    fn add_assign(&mut self, rhs: IVector) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl ops::Sub<i32> for IVector {
    type Output = IVector;

    fn sub(mut self, rhs: i32) -> Self {
        self.x -= rhs;
        self.y -= rhs;
        self
    }
}

impl ops::SubAssign<i32> for IVector {
    fn sub_assign(&mut self, rhs: i32) {
        self.x -= rhs;
        self.y -= rhs;
    }
}

impl ops::Sub<IVector> for IVector {
    type Output = IVector;

    fn sub(mut self, rhs: IVector) -> Self {
        self.x -= rhs.x;
        self.y -= rhs.y;
        self
    }
}

impl ops::SubAssign<IVector> for IVector {
    fn sub_assign(&mut self, rhs: IVector) {
        self.x -= rhs.x;
        self.y -= rhs.y;
    }
}

impl ops::Rem<i32> for IVector {
    type Output = IVector;

    fn rem(mut self, rhs: i32) -> IVector {
        self.x = self.x % rhs;
        self.y = self.y % rhs;
        self
    }
}
