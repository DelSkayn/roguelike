use specs::{DispatcherBuilder, System, World, WorldExt};

#[derive(Default)]
pub struct DataBuilder<'a, 'b> {
    pub(crate) dispatch: DispatcherBuilder<'a, 'b>,
    pub(crate) world: World,
}

impl<'a, 'b> DataBuilder<'a, 'b> {
    pub fn new() -> Self {
        DataBuilder {
            dispatch: DispatcherBuilder::new(),
            world: World::new(),
        }
    }

    pub fn with<T>(self, system: T, name: &str, dep: &[&str]) -> Self
    where
        T: for<'c> System<'c> + Send + 'a,
    {
        DataBuilder {
            dispatch: self.dispatch.with(system, name, dep),
            world: self.world,
        }
    }

    pub fn insert<T>(self, resource: T) -> Self
    where
        T: Send + Sync + 'static,
    {
        let mut world = self.world;
        world.insert(resource);
        DataBuilder {
            dispatch: self.dispatch,
            world,
        }
    }
}
