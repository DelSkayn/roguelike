use specs::prelude::*;

/// A flag signifying the entity that the player controls.
#[derive(Debug, Default, Copy, Clone)]
pub struct PlayerFlag;

impl Component for PlayerFlag {
    type Storage = NullStorage<Self>;
}

/// A struct containing name data for a entity.
#[derive(Debug, Clone)]
pub struct Name {
    pub first_name: String,
    pub last_name: String,
    pub nicknames: Vec<String>,
    pub titles: Vec<String>,
}

impl Default for Name {
    fn default() -> Self {
        Name {
            first_name: "Jane".to_string(),
            last_name: "Doe".to_string(),
            nicknames: Vec::new(),
            titles: vec!["The default one".to_string()],
        }
    }
}

impl Component for Name {
    type Storage = DenseVecStorage<Self>;
}
