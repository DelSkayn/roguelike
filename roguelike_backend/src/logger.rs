use backtrace;
use log::{self, Level, Log, Metadata, Record};
use quick_error::quick_error;
use std::{
    collections::VecDeque,
    fs::{File, OpenOptions},
    io::{self, Write},
    panic,
    path::{Path, PathBuf},
    sync::Mutex,
};

quick_error! {
    #[derive(Debug)]
    pub enum LoggerError {
        Io(err: io::Error){
            cause(err) display("IO error: {}",err)
            from()
        }
        SetLogger(err: log::SetLoggerError){
            cause(err)
            display("setting logger error: {}",err)
            from()
        }
    }
}

#[derive(Deserialize)]
pub struct LogFilter {
    filter: String,
    level: Level,
}

/// Config for logging.
#[derive(Deserialize)]
pub struct LogConfig {
    file: Option<PathBuf>,
    level: Option<Level>,
    stdio: bool,
    #[serde(default)]
    filters: Vec<LogFilter>,
}

impl Default for LogConfig {
    fn default() -> Self {
        LogConfig {
            file: Some(Path::new("log.txt").to_path_buf()),
            level: Some(Level::Info),
            stdio: false,
            filters: vec![],
        }
    }
}

pub struct Logger {
    level: Level,
    file: Option<Mutex<File>>,
    stdio: bool,
    filters: Vec<LogFilter>,
}

impl Log for Logger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= self.level
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            if let Some(x) = record.module_path() {
                if let Some(x) = self.filters.iter().find(|s| x.starts_with(&s.filter)) {
                    if record.metadata().level() > x.level {
                        return;
                    }
                }
            }

            let level = match record.level() {
                Level::Info => "\u{001b}[30;42mINFO \u{001b}[m",
                Level::Error => "\u{001b}[30;41mERROR\u{001b}[m",
                Level::Debug => "\u{001b}[30;47mDEBUG\u{001b}[m",
                Level::Trace => "\u{001b}[30;44mTRACE\u{001b}[m",
                Level::Warn => "\u{001b}[30;43mWARN \u{001b}[m",
            };

            if let Some(x) = record.module_path() {
                println!("{} [{}]:{}", level, x, record.args());
            } else {
                println!("{} :{}", level, record.args());
            }
            if let Some(file) = self.file.as_ref() {
                if let Some(x) = record.module_path() {
                    writeln!(
                        file.lock().unwrap(),
                        "{} [{}]:{}",
                        record.level(),
                        x,
                        record.args()
                    )
                    .unwrap();
                } else {
                    writeln!(
                        file.lock().unwrap(),
                        "{} :{}",
                        record.level(),
                        record.args()
                    )
                    .unwrap();
                }
            }
        }
    }

    fn flush(&self) {
        self.file
            .as_ref()
            .map(|x| x.lock().unwrap().flush().unwrap());
    }
}

impl Logger {
    pub fn init(config: LogConfig) -> Result<(), LoggerError> {
        if config.level.is_none() || (config.file.is_none() && !config.stdio) {
            return Ok(());
        }
        let level = config.level.unwrap();
        let file = if let Some(x) = config.file {
            let file = OpenOptions::new()
                .write(true)
                .truncate(true)
                .create(true)
                .open(x)?;
            Some(Mutex::new(file))
        } else {
            None
        };
        let logger = Logger {
            level,
            file,
            stdio: config.stdio,
            filters: config.filters,
        };

        log::set_max_level(level.to_level_filter());
        log::set_boxed_logger(Box::new(logger))?;

        panic::set_hook(Box::new(|info| {
            let payload;
            if let Some(x) = info.payload().downcast_ref::<String>().cloned() {
                payload = x;
            } else {
                payload = info
                    .payload()
                    .downcast_ref::<&str>()
                    .map(|x| x.to_string())
                    .unwrap();
            };

            let mut bc = Vec::new();
            backtrace::trace(|frame| {
                backtrace::resolve_frame(frame, |symbol| {
                    let unkown = "UKNOWN";
                    let text = format!(
                        "[{}] {}\n@ {}:{}\n",
                        symbol
                            .addr()
                            .map(|x| format!("{:?}", x))
                            .as_ref()
                            .map(|x| x.as_str())
                            .unwrap_or(unkown),
                        symbol
                            .name()
                            .map(|x| format!("{}", x))
                            .as_ref()
                            .map(|x| x.as_str())
                            .unwrap_or(unkown),
                        symbol.filename().and_then(|x| x.to_str()).unwrap_or(unkown),
                        symbol
                            .lineno()
                            .map(|x| format!("{}", x))
                            .as_ref()
                            .map(|x| x.as_str())
                            .unwrap_or(unkown)
                    );
                    bc.push(text);
                });
                true
            });

            let mut backtrace = String::new();
            for (idx, frame) in bc.drain(..).enumerate() {
                backtrace.push_str(&format!("{}: ", idx));
                backtrace.push_str(&frame);
            }

            let location = info
                .location()
                .map(|x| format!("{}", x))
                .unwrap_or("UNKNOWN".to_string());

            error!("PANIC: {}\n@ {}", payload, location);
            error!(" === BACKTRACE === ");
            error!("\n{}", backtrace);
        }));
        info!("initialized screen logger");
        info!("logging level: {}", level);
        Ok(())
    }
}
