use crossterm::Color;
use specs::{Read, System, WriteExpect};
use std::collections::VecDeque;

const MAX_PLAYER_MESSAGE: usize = 100;

#[derive(Default)]
pub struct PlayerLog {
    messages: VecDeque<String>,
}

impl PlayerLog {
    pub fn new() -> Self {
        PlayerLog {
            messages: VecDeque::with_capacity(MAX_PLAYER_MESSAGE),
        }
    }

    pub fn add_message(&mut self, text: &str) {
        for line in text.lines() {
            if self.messages.len() + 1 > MAX_PLAYER_MESSAGE {
                self.messages.pop_back();
            }
            self.messages.push_front(line.to_string());
        }
    }
}

