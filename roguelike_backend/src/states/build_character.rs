use crate::{
    app::prelude::*,
    entity::base::{Name, PlayerFlag},
    states::MainState,
};
use immers::Patchable;
use roguelike_common::msg::{
    self,
    build_character::{self, *},
};
use specs::prelude::*;

#[derive(Default)]
pub struct BuildCharacter {
    character: Character,
}

impl BuildCharacter {
    fn add_character(&self, world: &mut World) {
        <(ReadStorage<Name>, ReadStorage<PlayerFlag>)>::setup(world);
        let builder = world.create_entity();
        builder
            .with(Name {
                first_name: self.character.first_name.clone(),
                last_name: self.character.last_name.clone(),
                nicknames: Vec::new(),
                titles: vec!["the first".to_string()],
            })
            .with(PlayerFlag)
            .build();
    }
}

impl<F: Frontend> State<F> for BuildCharacter {
    fn on_start(&mut self, _world: &mut World, _ctx: &mut Ctx<F>) -> Transition<F> {
        Transition::Wait
    }

    fn on_message(
        &mut self,
        message: msg::Client,
        world: &mut World,
        ctx: &mut Ctx<F>,
    ) -> Transition<F> {
        let msg = if let msg::Client::BuildCharacter(x) = message {
            x
        } else {
            warn!("recieved invalid message!");
            return Transition::Wait;
        };

        let mut send = |x| ctx.send(msg::Server::BuildCharacter(x));

        match msg {
            Client::Patch(p) => {
                self.character
                    .apply(p.clone())
                    .map_err(|e| error!("invalid patch! {}", e))
                    .ok();
                send(Server::Patch(p))
            }
            Client::Cancel => {
                send(Server::Cancel);
                return Transition::Pop;
            }
            Client::Confirm => {
                self.add_character(world);
                send(Server::Confirm);
                return Transition::Replace(Box::new(MainState::default()));
            }
        }
        return Transition::Wait;
    }

    fn init(&self, _world: &mut World, ctx: &mut Ctx<F>) {
        ctx.send(msg::Server::Init(msg::init::Server::BuildCharacter));
        let mut send = |x| ctx.send(msg::Server::BuildCharacter(x));
        use msg::build_character::{Server::*, Stat};
        if let Some(x) = Character::default().produce(&self.character) {
            send(Patch(x));
        }
    }
}
