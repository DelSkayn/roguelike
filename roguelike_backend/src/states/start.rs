/// The state corresponding to the starting screen
pub struct StartState;

use crate::{app::prelude::*, states::build_character::BuildCharacter};
use roguelike_common::msg;

impl<F: Frontend> State<F> for StartState {
    fn on_start(&mut self, _world: &mut World, _ctx: &mut Ctx<F>) -> Transition<F> {
        Transition::Wait
    }

    fn on_message(
        &mut self,
        message: msg::Client,
        _world: &mut World,
        ctx: &mut Ctx<F>,
    ) -> Transition<F> {
        let msg = if let msg::Client::Start(x) = message {
            x
        } else {
            panic!("recieved invalid message: {:#?}", message);
        };
        let mut send = |x| ctx.send(msg::Server::Start(x));

        use msg::start::Client::*;
        match msg {
            Quit => Transition::Quit,
            NewGame => {
                send(msg::start::Server::NewGame);
                Transition::Push(Box::new(BuildCharacter::default()))
            }
            _ => Transition::Wait,
        }
    }

    fn tick(&mut self, _world: &mut World, _ctx: &mut Ctx<F>) -> Transition<F> {
        Transition::Wait
    }

    fn init(&self, _world: &mut World, ctx: &mut Ctx<F>) {
        ctx.send(msg::Server::Init(msg::init::Server::Start));
    }
}
