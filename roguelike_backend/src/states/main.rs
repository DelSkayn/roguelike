use crate::{
    app::prelude::*,
    entity::base::{Name, PlayerFlag},
    story::Story,
};
use immers::Patchable;
use roguelike_common::msg::{
    self,
    main::{Client, PlayerInfo, Server, StoryState},
};
use specs::prelude::*;

#[derive(Default)]
pub struct MainState {
    player_info: PlayerInfo,
    cur_story_state: StoryState,
}

impl MainState {
    pub fn update_state(&mut self, read: ReadExpect<Story>) {
        self.cur_story_state = StoryState {
            text: read.get_text(),
            options: read.get_options(),
        };
    }
}

fn update_player_info(s: &mut PlayerInfo, world: &World) {
    let (flag, name) = world.system_data::<(ReadStorage<PlayerFlag>, ReadStorage<Name>)>();
    if let Some((_, name)) = (&flag, &name).join().next() {
        s.last_name = name.last_name.clone();
        s.first_name = name.first_name.clone();
    } else {
        panic!("No player!");
    }
}

impl<F: Frontend> State<F> for MainState {
    fn on_start(&mut self, world: &mut World, ctx: &mut Ctx<F>) -> Transition<F> {
        update_player_info(&mut self.player_info, world);
        if let Some(patches) = PlayerInfo::default().produce(&self.player_info) {
            ctx.send(msg::Server::Main(Server::PlayerInfo(patches)));
        }
        self.update_state(world.system_data());
        ctx.send(msg::Server::Main(Server::StateChange(
            self.cur_story_state.clone(),
        )));
        Transition::Wait
    }

    fn on_message(
        &mut self,
        message: msg::Client,
        world: &mut World,
        ctx: &mut Ctx<F>,
    ) -> Transition<F> {
        let msg = if let msg::Client::Main(x) = message {
            x
        } else {
            warn!("recieved invalid message!");
            return Transition::Wait;
        };
        let mut send = |x| ctx.send(msg::Server::Main(x));
        match msg {
            Client::Exit => {
                world.delete_all();
                send(Server::Exit);
                return Transition::Pop;
            }
            Client::TakeOption(x) => {
                world.exec(|mut s: WriteExpect<Story>| {
                    s.take_option(x);
                });
                self.update_state(world.system_data());
                send(Server::StateChange(self.cur_story_state.clone()));
            }
        }
        Transition::Wait
    }

    fn init(&self, _world: &mut World, ctx: &mut Ctx<F>) {
        ctx.send(msg::Server::Init(msg::init::Server::Main));
        let mut send = |x| ctx.send(msg::Server::Main(x));
        if let Some(x) = PlayerInfo::default().produce(&self.player_info) {
            send(Server::PlayerInfo(x));
        }
        send(Server::StateChange(self.cur_story_state.clone()));
    }
}
