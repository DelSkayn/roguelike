//! This crate implements a frontend which can use the browser as an interface.
//! It implements a simple static fileserver with a websocket server for sending message to and
//! from the client.

#![allow(dead_code)]
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate quick_error;
#[macro_use]
extern crate log;

use open;

mod lock;
mod server;
mod ws;
use websocket::OwnedMessage;

use roguelike_common::{msg, Frontend};

fn default_static_path() -> String {
    "res".to_string()
}

/// Config options for the web frontend
#[derive(Deserialize)]
pub struct WebFrontendConfig {
    /// The address the server hosts on
    pub addr: String,
    /// The port the server hosts on
    pub port: u16,
    /// The path of the static files to be send to the webbrowser
    #[serde(default = "default_static_path")]
    pub static_path: String,
    /// Open a browser on starting the webfrontend
    pub open_browser: bool,
}

impl Default for WebFrontendConfig {
    fn default() -> Self {
        WebFrontendConfig {
            addr: "127.0.0.1".to_string(),
            port: 8080,
            static_path: "res".to_string(),
            open_browser: false,
        }
    }
}

/// The web frontend
pub struct WebFrontend {
    server: server::Server,
}

impl WebFrontend {
    /// Create a new webfrontend.
    /// This also starts a server and optionally opens the browser on the address on which the
    /// server is hosted.
    pub fn new(config: WebFrontendConfig) -> Self {
        let addr = config.addr.clone();
        let mut server = server::Server::new(config.addr, config.port, config.static_path);
        server.start().unwrap();
        if config.open_browser {
            info!("opening browser!");
            open::that(format!("http://{}:{}", addr, config.port)).unwrap();
        }
        WebFrontend { server }
    }

    fn map_websocket(msg: OwnedMessage) -> Option<msg::Client> {
        if let OwnedMessage::Binary(x) = msg {
            bincode::deserialize(&x)
                .map_err(|e| {
                    warn!("could not deserialize message: {}", e);
                    ()
                })
                .ok()
        } else {
            warn!("recieved invalid websocket message: {:?}", msg);
            None
        }
    }
}

impl Frontend for WebFrontend {
    fn try_recv(&mut self) -> Option<msg::Client> {
        while let Some(x) = self.server.try_recv() {
            if let Some(x) = Self::map_websocket(x) {
                return Some(x);
            }
        }
        None
    }

    fn recv(&mut self) -> msg::Client {
        loop {
            let x = self.server.recv();
            if let Some(x) = Self::map_websocket(x) {
                return x;
            }
        }
    }

    fn send(&mut self, msg: msg::Server) {
        let msg = bincode::serialize(&msg).unwrap();
        self.server.send(OwnedMessage::Binary(msg));
    }
}
