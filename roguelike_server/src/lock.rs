use std::{
    cell::UnsafeCell,
    ops::{Deref, DerefMut},
    sync::atomic::{AtomicBool, Ordering},
};

/// A simple ""lock free" value lock.
/// Does not allow blocking
pub struct Lock<T> {
    is_locked: AtomicBool,
    value: UnsafeCell<T>,
}

impl<T> Lock<T> {
    pub fn new(value: T) -> Self {
        Lock {
            is_locked: AtomicBool::new(false),
            value: UnsafeCell::new(value),
        }
    }

    /// Test if the value is locked
    pub fn is_locked(&self) -> bool {
        self.is_locked.load(Ordering::Acquire)
    }

    /// Try locking a value
    pub fn try_lock(&self) -> Option<LockGuard<'_, T>> {
        if self
            .is_locked
            .compare_and_swap(false, true, Ordering::AcqRel)
        {
            return None;
        }
        unsafe {
            Some(LockGuard {
                lock_ref: &self.is_locked,
                value: &mut *self.value.get(),
            })
        }
    }
}

pub struct LockGuard<'a, T> {
    lock_ref: &'a AtomicBool,
    value: &'a mut T,
}

impl<T> Deref for LockGuard<'_, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        self.value
    }
}

impl<T> DerefMut for LockGuard<'_, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.value
    }
}

impl<T> Drop for LockGuard<'_, T> {
    fn drop(&mut self) {
        self.lock_ref.store(false, Ordering::Release);
    }
}

unsafe impl<T> Send for Lock<T> {}
unsafe impl<T> Sync for Lock<T> {}
