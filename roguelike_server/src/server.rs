use crate::{lock::Lock, ws};
use hyper::{
    service::{make_service_fn, Service},
    Body, Request, Response, Server as HyperServer, StatusCode,
};
use std::{
    future::Future,
    net::IpAddr,
    path::{Path, PathBuf},
    pin::Pin,
    sync::Arc,
    task::{Context, Poll},
    thread::{self, JoinHandle},
};
use tokio::{
    fs::File,
    io::{AsyncReadExt, ErrorKind},
    runtime,
    sync::mpsc::{self, Receiver, Sender},
    sync::oneshot::{self, Sender as ShutdownSender},
};
use tokio_util::codec::Framed;
use websocket::{
    codec::{Context as WsContext, MessageCodec},
    OwnedMessage,
};
use ws::WebSocketClient;

quick_error! {
    #[derive(Debug)]
    pub enum Error{
        Http(e: http::Error){
            from()
            display("http error: {}",e)
            cause(e)
        }
        Tokio(e: tokio::io::Error){
            from()
            display("tokio IO error: {}",e)
            cause(e)
        }
    }
}

static BAD_REQUEST: &'static [u8] = b"Bad Request";
//static INTERNAL_SERVER_ERROR: &'static [u8] = b"Internal Server Error";
static NOT_FOUND: &'static [u8] = b"Not Found";

#[derive(Clone)]
pub struct WebService {
    /// the path to load files from
    path: PathBuf,
    message: Arc<MessagingChannels>,
}

impl WebService {
    async fn bad_request() -> Result<Response<Body>, Error> {
        Response::builder()
            .status(StatusCode::BAD_REQUEST)
            .body(BAD_REQUEST.into())
            .map_err(Error::Http)
    }

    async fn not_found() -> Result<Response<Body>, Error> {
        Response::builder()
            .status(StatusCode::NOT_FOUND)
            .body(NOT_FOUND.into())
            .map_err(Error::Http)
    }

    fn get_content_type(ext: &str) -> Option<&'static str> {
        match ext {
            "html" | "htm" => Some("text/html"),
            "css" => Some("text/css"),
            "jpeg" | "jpg" => Some("image/jpeg"),
            "js" => Some("text/javascript"),
            "json" => Some("application/json"),
            "png" => Some("image/png"),
            "wasm" => Some("application/wasm"),
            _ => None,
        }
    }

    async fn load_file(path: PathBuf) -> Result<Response<Body>, Error> {
        trace!("loading file: {:?}", path);
        let file = File::open(&path).await;
        let mut file = match file {
            Ok(x) => x,
            Err(e) => {
                match e.kind() {
                    ErrorKind::NotFound | ErrorKind::PermissionDenied => {
                        return Self::not_found().await
                    }
                    _ => {}
                }
                return Err(e.into());
            }
        };
        let mut buffer = Vec::new();
        file.read_to_end(&mut buffer).await?;
        let body = Body::from(buffer);

        let mut builder = Response::builder().status(200);
        let content_type = path
            .extension()
            .and_then(|x| x.to_str())
            .and_then(Self::get_content_type);

        if let Some(x) = content_type {
            builder = builder.header("Content-Type", x)
        }

        builder.body(body).map_err(Error::Http)
    }

    async fn upgrade_websocket(
        req: Request<Body>,
        msg: Arc<MessagingChannels>,
    ) -> Result<Response<Body>, Error> {
        if msg.send.is_locked() || msg.recv.is_locked() {
            warn!("websocket connection already present, rejecting new connection");
            return Self::bad_request().await;
        }

        // Validate all the headers
        let headers = req.headers();
        if headers
            .get(http::header::UPGRADE)
            .map(|x| x != "websocket")
            .unwrap_or(true)
        {
            return Self::bad_request().await;
        }
        if headers
            .get(http::header::CONNECTION)
            .and_then(|x| x.to_str().ok())
            .map(|x| !x.to_lowercase().contains("upgrade"))
            .unwrap_or(true)
        {
            return Self::bad_request().await;
        }
        if headers
            .get(http::header::SEC_WEBSOCKET_VERSION)
            .map(|x| x != "13")
            .unwrap_or(true)
        {
            return Self::bad_request().await;
        }
        if !headers.contains_key(http::header::SEC_WEBSOCKET_KEY) {
            return Self::bad_request().await;
        }

        let sec_key = headers
            .get(http::header::SEC_WEBSOCKET_KEY)
            .unwrap()
            .to_str();
        let sec_key = match sec_key {
            Ok(x) => x,
            Err(_) => return Self::bad_request().await,
        };

        // Create a response
        let res = Response::builder()
            .status(StatusCode::SWITCHING_PROTOCOLS)
            .header(http::header::UPGRADE, "websocket")
            .header(http::header::CONNECTION, "Upgrade")
            .header(
                http::header::SEC_WEBSOCKET_ACCEPT,
                ws::convert_sec_key(sec_key),
            )
            .body(Body::empty())?;

        // Start the client
        tokio::spawn(async {
            let upgrade = match req.into_body().on_upgrade().await {
                Ok(x) => x,
                Err(e) => {
                    warn!("failed to upgrade channel: {}", e);
                    return;
                }
            };
            let stream = Framed::new(upgrade, MessageCodec::default(WsContext::Server));
            let client = WebSocketClient::new(stream, msg);
            client.run();
        });

        Ok(res)
    }
}

impl Service<Request<Body>> for WebService {
    type Response = Response<Body>;
    type Error = Error;
    type Future = Pin<Box<dyn Future<Output = Result<Self::Response, Self::Error>> + Send>>;

    fn poll_ready(&mut self, _: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }

    fn call(&mut self, req: Request<Body>) -> Self::Future {
        trace!("Request: {:#?}", req);
        // Check for websocket upgrade
        if req.headers().contains_key(http::header::UPGRADE) {
            return Box::pin(Self::upgrade_websocket(req, self.message.clone()));
        }

        // Check for files
        let mut path = req.uri().path();
        if path == "/" {
            path = "index.html";
        }
        let path = Path::new(path);
        let path = self.path.join(path.strip_prefix("/").unwrap_or(path));
        let path_canon = path.canonicalize();
        let path = match path_canon {
            Ok(x) => x,
            Err(e) => {
                warn!(
                    "error canonicalizeing path {}, {}",
                    path.to_str().unwrap_or("NOT UTF8"),
                    e
                );
                return Box::pin(Self::not_found());
            }
        };

        let is_child_path = self
            .path
            .iter()
            .zip(path.iter())
            .fold(true, |acc, (a, b)| acc && (a == b));

        if !is_child_path {
            warn!("invalid path!");
            return Box::pin(Self::not_found());
        }

        Box::pin(Self::load_file(path))
    }
}

pub struct MessagingChannels {
    pub recv: Lock<Receiver<OwnedMessage>>,
    pub send: Lock<Sender<OwnedMessage>>,
}

pub struct Server {
    shutdown_channel: Option<ShutdownSender<()>>,
    handle: Option<JoinHandle<()>>,
    addr: String,
    port: u16,
    file_loc: PathBuf,
    recv: Receiver<OwnedMessage>,
    send: Sender<OwnedMessage>,
    msg: Arc<MessagingChannels>,
}

impl Server {
    pub fn new<P: AsRef<Path>>(addr: String, port: u16, file_loc: P) -> Self {
        let file_loc = file_loc.as_ref().canonicalize().unwrap_or(file_loc.as_ref().to_path_buf());
        let (a_send, a_recv) = mpsc::channel(16);
        let (b_send, b_recv) = mpsc::channel(16);

        let channel = MessagingChannels {
            send: Lock::new(a_send),
            recv: Lock::new(b_recv),
        };

        let msg = Arc::new(channel);

        Server {
            file_loc,
            addr,
            port,
            shutdown_channel: None,
            recv: a_recv,
            send: b_send,
            msg,
            handle: None,
        }
    }

    pub fn send(&mut self, msg: OwnedMessage) {
        if !self.is_connected() {
            return;
        }

        futures::executor::block_on(self.send.send(msg)).unwrap();
    }

    pub fn try_recv(&mut self) -> Option<OwnedMessage> {
        self.recv.try_recv().ok()
    }

    pub fn recv(&mut self) -> OwnedMessage {
        futures::executor::block_on(self.recv.recv()).unwrap()
    }

    pub fn start(&mut self) -> Result<(), Error> {
        info!("starting server on {}:{}", self.addr, self.port);
        let (shutdown_send, shutdown_recv) = oneshot::channel();

        let file_loc = self.file_loc.clone();

        let ip: IpAddr = self.addr.parse().unwrap();
        let port = self.port.clone();
        let message = self.msg.clone();
        self.handle = Some(thread::spawn(move || {
            let mut runtime = runtime::Builder::new()
                .enable_all()
                .basic_scheduler()
                .build()
                .unwrap();

            let service = make_service_fn(move |_| {
                let file_loc = file_loc.clone();
                let msg = message.clone();
                async move {
                    Ok::<_, Error>(WebService {
                        path: file_loc,
                        message: msg,
                    })
                }
            });
            //TODO Error handling.
            let server = runtime.enter(|| {
                HyperServer::try_bind(&(ip, port).into())
                    .unwrap()
                    .serve(service)
                    .with_graceful_shutdown(async {
                        shutdown_recv.await.ok();
                    })
            });

            runtime.block_on(server).unwrap();
        }));
        self.shutdown_channel = Some(shutdown_send);
        Ok(())
    }

    pub fn is_connected(&self) -> bool {
        self.msg.send.is_locked()
    }
}

impl Drop for Server {
    fn drop(&mut self) {
        if let Some(s) = self.shutdown_channel.take() {
            trace!("shutting down server");
            s.send(()).ok();
            self.handle.take().unwrap().join().ok();
        }
    }
}
