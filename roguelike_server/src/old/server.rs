use super::{AsyncReceiver, Sender};
use futures_util::{
    future::{self, Either},
    stream::StreamExt,
};
use hyper::{
    header::{self, HeaderValue},
    service::{make_service_fn, service_fn},
    upgrade::Upgraded,
    Body, Request, Response, Result, Server, StatusCode,
};
use roguelike_common::{ClientMessage, ServerMessage};
use std::path::{Path, PathBuf};
use tokio::{
    self,
    fs::File,
    io::AsyncReadExt,
    runtime,
    sync::{mpsc, oneshot::Receiver as OneReceiver},
};
use tokio_codec::Framed;
use websocket::{
    codec::{Context, MessageCodec},
    OwnedMessage as Message,
};

use base64;
use sha1::Sha1;
use std::{cell::RefCell, rc::Rc};

static BAD_REQUEST: &'static [u8] = b"Bad Request";
static INTERNAL_SERVER_ERROR: &'static [u8] = b"Internal Server Error";
static NOT_FOUND: &'static [u8] = b"Not Found";

type WsReciever = Rc<RefCell<Option<AsyncReceiver<ServerMessage>>>>;
type WsSender = Rc<RefCell<Sender<ClientMessage>>>;

fn bad_request() -> Response<Body> {
    Response::builder()
        .status(StatusCode::BAD_REQUEST)
        .body(BAD_REQUEST.into())
        .unwrap()
}

fn not_found() -> Response<Body> {
    Response::builder()
        .status(StatusCode::NOT_FOUND)
        .body(NOT_FOUND.into())
        .unwrap()
}

/// HTTP status code 500
fn internal_server_error() -> Response<Body> {
    Response::builder()
        .status(StatusCode::INTERNAL_SERVER_ERROR)
        .body(INTERNAL_SERVER_ERROR.into())
        .unwrap()
}

async fn send_file(path: PathBuf, req: Request<Body>) -> Result<Response<Body>> {
    let new_path = if req.uri().path() != "/" {
        let uri_file_path = req.uri().path().trim_start_matches("/");
        if uri_file_path.contains("..") {
            return Ok(bad_request());
        }
        path.join(uri_file_path)
    } else {
        path.join("index.html")
    };
    trace!("serving file: {:?}", new_path);
    if let Ok(mut file) = File::open(&new_path).await {
        let mut buf = Vec::new();
        if let Ok(_) = file.read_to_end(&mut buf).await {
            let mut res = Response::new(buf.into());
            if let Some(ext) = new_path.extension().and_then(|x| x.to_str()) {
                let h = res.headers_mut();
                match ext {
                    "js" => {
                        h.insert(
                            header::CONTENT_TYPE,
                            "application/javascript".parse().unwrap(),
                        );
                    }
                    "wasm" => {
                        h.insert(header::CONTENT_TYPE, "application/wasm".parse().unwrap());
                    }
                    "html" => {
                        h.insert(header::CONTENT_TYPE, "text/html".parse().unwrap());
                    }
                    _ => {}
                };
            }
            return Ok(res);
        }

        return Ok(internal_server_error());
    }

    trace!("no such file");
    Ok(not_found())
}

async fn handle_websocket(upgraded: Upgraded, recv: WsReciever, send: WsSender) -> Result<()> {
    //TODO handle error
    let codec = MessageCodec::new(Context::Server);
    let (mut sink, mut stream) = Framed::new(upgraded, codec).split();

    // TODO handle ping pong.
    let (mut p_send, mut p_recv) = mpsc::channel(100);
    let sink = async move {
        //let value = recv.borrow_mut()
        let mut reciever = recv.borrow_mut().take().unwrap();
        loop {
            let r = reciever.next();
            let p_r = p_recv.next();
            let value = future::select(r, p_r).await;
            match value {
                Either::Left((x, _)) => {
                    if x.is_none() {
                        trace!("outgoing message reciever closed");
                        break;
                    }
                    sink.send(Message::Binary(bincode::serialize(&x.unwrap()).unwrap()))
                        .await
                        .unwrap();
                }
                Either::Right((x, _)) => {
                    if x.is_none() {
                        trace!("outgoing ping reciever closed");
                        break;
                    }
                    sink.send(Message::Pong(x.unwrap())).await.unwrap();
                }
            };
        }
        recv.borrow_mut().replace(reciever);
    };
    let stream = async move {
        loop {
            let value = stream.next().await;
            if value.is_none() {
                trace!("incomming websocket socket closed");
                break;
            }
            let value = value.unwrap();
            match value {
                Ok(Message::Binary(x)) => {
                    match bincode::deserialize(&x) {
                        Ok(x) => {
                            trace!("recieved message: {:#?}", x);
                            send.borrow_mut().send(x).unwrap();
                        }
                        Err(e) => {
                            debug!("error deserializing recieved message: {}", e);
                        }
                    };
                }
                Ok(Message::Close(reason)) => {
                    if let Some(x) = reason {
                        trace!("websocket closed: {}", x.reason);
                    } else {
                        trace!("websocket closed");
                    }
                    break;
                }
                Ok(Message::Ping(x)) => {
                    p_send.send(x).await.unwrap();
                }
                Err(e) => {
                    error!("websocket error: {}", e);
                }
                _ => {}
            }
        }
    };
    future::join(sink, stream).await;
    Ok(())
}

fn convert_key(input: &[u8]) -> String {
    // ... field is constructed by concatenating /key/ ...
    // ... with the string "258EAFA5-E914-47DA-95CA-C5AB0DC85B11" (RFC 6455)
    const WS_GUID: &[u8] = b"258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
    let mut sha1 = Sha1::default();
    sha1.update(input);
    sha1.update(WS_GUID);
    base64::encode(&sha1.digest().bytes())
}

async fn upgrade_websocket(
    req: Request<Body>,
    recv: WsReciever,
    sender: WsSender,
) -> Result<Response<Body>> {
    trace!("recieved websocket request");
    let mut res = Response::new(Body::empty());

    let headers = req.headers();
    if !headers
        .get(header::UPGRADE)
        .and_then(|x| x.to_str().ok())
        .map(|x| x.to_lowercase().contains("websocket"))
        .unwrap_or(false)
    {
        debug!("invalid websocket request: missing \"Upgrade: websocket\" header");
        return Ok(bad_request());
    }

    if !headers
        .get(header::CONNECTION)
        .and_then(|x| x.to_str().ok())
        .map(|x| x.to_lowercase().contains("upgrade"))
        .unwrap_or(false)
    {
        debug!("invalid websocket request: missing \"Connection: upgrade\" header");
        return Ok(bad_request());
    }

    if !headers
        .get(header::SEC_WEBSOCKET_VERSION)
        .map(|x| x == "13")
        .unwrap_or(false)
    {
        debug!("invalid websocket request: missing websocket version header");
        return Ok(bad_request());
    }
    if !headers.contains_key(header::SEC_WEBSOCKET_KEY) {
        debug!("invalid websocket request: missing websocket security key");
        return Ok(bad_request());
    }

    if recv.try_borrow_mut().map(|x| x.is_none()).unwrap_or(true) {
        debug!("websocket connection requested but one was already in use");
        return Ok(bad_request());
    }

    let key = if let Ok(x) = headers.get(header::SEC_WEBSOCKET_KEY).unwrap().to_str() {
        convert_key(x.as_bytes())
    } else {
        return Ok(bad_request());
    };

    tokio::spawn(async move {
        match req.into_body().on_upgrade().await {
            Ok(upgraded) => {
                handle_websocket(upgraded, recv, sender).await.ok();
            }
            Err(e) => debug!("upgrade websocket error: {}", e),
        };
    });

    *res.status_mut() = StatusCode::SWITCHING_PROTOCOLS;
    res.headers_mut()
        .insert(header::UPGRADE, HeaderValue::from_static("websocket"));
    res.headers_mut()
        .insert(header::CONNECTION, HeaderValue::from_static("upgrade"));
    res.headers_mut().insert(
        header::SEC_WEBSOCKET_ACCEPT,
        HeaderValue::from_str(key.as_str()).unwrap(),
    );
    Ok(res)
}

pub fn run(
    addr: &str,
    file_dir: &str,
    recv: AsyncReceiver<ServerMessage>,
    send: Sender<ClientMessage>,
    close: OneReceiver<()>,
) {
    let path = file_dir.to_string();
    info!("server front @ {}", path);
    warn!("check if tungstenite tokio merged pull request");

    let recv = Rc::new(RefCell::new(Some(recv)));
    let send = Rc::new(RefCell::new(send));

    let service = make_service_fn(move |_| {
        let path_clone = Path::new(&path).to_path_buf();
        let recv_clone = recv.clone();
        let send_clone = send.clone();
        async move {
            Ok::<_, hyper::Error>(service_fn(move |req: Request<Body>| {
                let path_c = path_clone.clone();
                let recv_c = recv_clone.clone();
                let send_c = send_clone.clone();
                async move {
                    trace!("requested: {}", req.uri());
                    match req.uri().path() {
                        "/ws" => upgrade_websocket(req, recv_c, send_c).await,
                        _ => send_file(path_c, req).await,
                    }
                }
            }))
        }
    });

    let addr = addr.to_string();
    let mut runtime = runtime::Builder::new().basic_scheduler();
    runtime.block_on(async move {
        info!("starting server on: {}", addr);

        let server = Server::bind(&addr.parse().unwrap())
            .serve(service)
            .with_graceful_shutdown(async {
                close.await.ok();
            });
        if let Err(e) = server.await {
            error!("server error: {}", e);
        }
        info!("shuting down server");
    });
}
