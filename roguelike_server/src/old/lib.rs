//! This crate implements a simple web server with a web socket channel for sending messages.

#[macro_use]
extern crate log;
#[macro_use]
extern crate serde_derive;

use crossbeam::channel::{Receiver, Sender};
use quick_error::quick_error;
use roguelike_common::{ClientMessage, Frontend, ServerMessage};
use std::{
    error::Error,
    io::Error as IOError,
    thread::{self, JoinHandle},
};
use tokio::sync::{mpsc, oneshot};

type AsyncSender<T> = mpsc::UnboundedSender<T>;
type AsyncReceiver<T> = mpsc::UnboundedReceiver<T>;

mod server;

fn async_channel<T>() -> (AsyncSender<T>, AsyncReceiver<T>) {
    mpsc::unbounded_channel()
}

fn channel<T>() -> (Sender<T>, Receiver<T>) {
    crossbeam::channel::unbounded()
}

//mod file_server;
//use file_server::{Config as FileConfig, FileServer};
//mod ws_server;
//use ws_server::WsServer;

quick_error! {
    #[derive(Debug)]
    pub enum FrontError{
        Server(err: Box<dyn Error + Send + Sync + 'static>){
            display("server error: {}",err)
        }
        IoError(err: IOError){
            from()
            cause(err)
            display("IO error: {}",err)
        }
    }
}

fn default_file_location() -> String {
    "res".to_string()
}

#[derive(Deserialize, Default)]
pub struct Config {
    pub addr: String,
    #[serde(default = "default_file_location")]
    pub file_location: String,
}

pub struct WebFrontend {
    sender: AsyncSender<ServerMessage>,
    recv: Receiver<ClientMessage>,
    handle: Option<JoinHandle<()>>,
    close: Option<oneshot::Sender<()>>,
}

impl WebFrontend {
    pub fn new(config: Config) -> Result<Self, FrontError> {
        // sent to the clie
        let (out_send, out_recv) = async_channel();
        let (in_send, in_recv) = channel();
        let (close_send, close_recv) = oneshot::channel();
        let addr = config.addr;
        let file_location = config.file_location;
        let handle = thread::spawn(move || {
            server::run(&addr, &file_location, out_recv, in_send, close_recv);
        });
        Ok(WebFrontend {
            sender: out_send,
            recv: in_recv,
            close: Some(close_send),
            handle: Some(handle),
        })
    }

    pub fn try_recv(&mut self) -> Option<ClientMessage> {
        let res = self.recv.try_recv();
        match res {
            Ok(x) => Some(x),
            Err(e) => {
                if e.is_empty() {
                    None
                } else {
                    panic!("Frontend server disconnected!");
                }
            }
        }
    }

    pub fn recv(&mut self) -> ClientMessage {
        self.recv.recv().unwrap()
    }

    pub fn send(&mut self, message: ServerMessage) {
        self.sender.try_send(message).unwrap();
    }
}

impl Frontend for WebFrontend {
    fn try_recv(&mut self) -> Option<ClientMessage> {
        (*self).try_recv()
    }

    fn recv(&mut self) -> ClientMessage {
        (*self).recv()
    }

    fn send(&mut self, msg: ServerMessage) {
        (*self).send(msg);
    }
}

impl Drop for WebFrontend {
    fn drop(&mut self) {
        self.close.take().unwrap().send(()).unwrap();
        self.handle.take().unwrap().join().unwrap();
    }
}
