use base64;
use futures::{select, Sink, Stream};
use futures_util::{sink::SinkExt, stream::StreamExt};
use sha1::Sha1;
use tokio::sync::mpsc;
use websocket::{OwnedMessage, WebSocketError};

use crate::server::MessagingChannels;

use std::{fmt, sync::Arc};

pub struct WebSocketClient<S> {
    pub stream: S,
    pub message: Arc<MessagingChannels>,
}

pub fn convert_sec_key(key: &str) -> String {
    // ... field is constructed by concatenating /key/ ...
    // ... with the string "258EAFA5-E914-47DA-95CA-C5AB0DC85B11" (RFC 6455)
    const WS_GUID: &[u8] = b"258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
    let mut sha1 = Sha1::default();
    sha1.update(key.as_bytes());
    sha1.update(WS_GUID);
    base64::encode(&sha1.digest().bytes())
}

impl<S> WebSocketClient<S>
where
    S: Stream<Item = Result<OwnedMessage, WebSocketError>> + Sink<OwnedMessage> + Send + 'static,
    S::Error: fmt::Display,
{
    pub fn new(stream: S, message: Arc<MessagingChannels>) -> Self {
        WebSocketClient { stream, message }
    }

    pub fn run(self) {
        let (mut ws_sink, mut ws_stream) = self.stream.split();
        let (mut pong_send, pong_recv) = mpsc::channel(10);
        let msg_sender = self.message.clone();
        let msg_reciever = self.message.clone();

        // handle pings and recieving messages
        tokio::spawn(async move {
            let mut msg_sender = if let Some(x) = msg_sender.send.try_lock() {
                x
            } else {
                return;
            };
            loop {
                let value = match ws_stream.next().await {
                    Some(x) => x,
                    None => return,
                };

                match value {
                    Ok(OwnedMessage::Ping(x)) => {
                        if let Err(_) = pong_send.send(OwnedMessage::Pong(x)).await {
                            trace!("websocket connection lost");
                            return;
                        }
                    }
                    Ok(OwnedMessage::Close(x)) => {
                        if let Some(x) = x {
                            trace!("websocket connection closed: {:?}", x.reason);
                        } else {
                            trace!("websocket connection closed");
                        }
                        return;
                    }
                    Err(e) => {
                        warn!("error decodeing websocket message: {}", e);
                        return;
                    }
                    Ok(x) => {
                        if let Err(_) = msg_sender.send(x).await {
                            return;
                        }
                    }
                }
            }
        });

        // send messages
        tokio::spawn(async move {
            let mut msg_reciever = if let Some(x) = msg_reciever.recv.try_lock() {
                x
            } else {
                return;
            };
            let mut msg_reciever = (&mut *msg_reciever).fuse();
            let mut pong_recv = pong_recv.fuse();
            loop {
                let res = select! {
                    pong = pong_recv.next() => {
                        if let Some(pong) = pong{
                            pong
                        }else{
                            break;
                        }
                    },
                    msg = msg_reciever.next() => {
                        if let Some(msg) = msg{
                            msg
                        }else{
                            break;
                        }
                    }
                };
                match ws_sink.send(res).await {
                    Ok(_) => {}
                    Err(e) => warn!("error sending pong message: {}", e),
                }
            }
        });
    }
}
