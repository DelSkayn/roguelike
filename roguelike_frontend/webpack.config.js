const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");
const WasmPackPlugin = require("@wasm-tool/wasm-pack-plugin");
const webpack = require("webpack");
const exec = require("child_process").exec;

const dist = path.resolve(__dirname, "dist");

module.exports = (env, argv) => {
  return {
    devServer: {
      contentBase: dist,
      compress: argv.mode === "production",
      port: 8000,
      proxy: {
        "/ws": {
          target: "ws://127.0.0.1:8080",
          ws: true
        }
      }
    },
    entry: "./js/index.js",
    output: {
      path: dist,
      filename: "front.js",
      webassemblyModuleFilename: "front.wasm"
    },
    plugins: [
      new CopyPlugin([{ from: "./static", to: dist }]),
      new WasmPackPlugin({
        crateDirectory: ".",
        extraArgs: "--no-typescript"
      }),
      {
        apply: compiler => {
          compiler.hooks.afterEmit.tap("AfterEmitPlugin", compilation => {
            exec('notify-send "webpack" "finished building front"', () => {});
          });
        }
      }
    ],
    watch: argv.mode !== "production",
    module: {
      rules: [
        {
          test: /\.css$/,
          use: [
            {
              loader: "style-loader"
            },
            {
              loader: "css-loader",
              options: {
                importLoaders: 1
              }
            },
            {
              loader: "postcss-loader"
            }
          ]
        }
      ]
    }
  };
};
