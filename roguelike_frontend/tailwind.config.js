module.exports = {
  important: true,
  theme: {
    extend: {
      colors: {
        "bg-darkest": "#1d2021",
        "bg-darker": "#282828",
        bg: "#32302f",
        fg: "#ebdbb2",
        white: "#ebdbb2",
        black: "#32302f",
        gray: "#a89984",
        accent: "#d65d0e",
        "accent-alt": "#af3a03",
        orange: "#d65d0e",
        "orange-alt": "#af3a03",
        red: "#cc241d",
        "red-alt": "#fb4934",
        green: "#98971a",
        "green-alt": "#b8bb26",
        yellow: "#d79921",
        "yellow-alt": "#fabd2f",
        blue: "#458588",
        "blue-alt": "#83a598",
        purple: "#b16286",
        "purple-alt": "#d3869b",
        aqua: "#689d6a",
        "aqua-alt": "#8ec07c"
      }
    }
  }
};
