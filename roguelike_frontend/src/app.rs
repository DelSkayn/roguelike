use crate::html;
use crate::states::*;
use crate::ws::{Ws, WsMsg};
//use roguelike_common::ClientMessage;
use roguelike_common::msg;
use seed::prelude::*;

pub mod prelude {
    pub use super::{AppState, Ctx, Event, Trans};
    pub use seed::prelude::*;
}

#[derive(Debug)]
pub struct Model {
    state: Vec<AppState>,
    ws: Option<Ws>,
}

pub enum Trans {
    Pop,
    Push(AppState),
    Replace(AppState),
    Continue,
}

#[derive(Debug)]
pub enum AppState {
    Init,
    Start(start::Model),
    BuildCharacter(build_character::Model),
    Main(main::Model),
    Quit(quit::Model),
}

impl AppState {
    fn is_init(&self) -> bool {
        match *self {
            AppState::Init => true,
            _ => false,
        }
    }

    fn handle(&self, event: Event) -> Option<Msg> {
        match *self {
            AppState::Init => None,
            AppState::Quit(_) => None,
            AppState::Start(_) => start::handle(event).map(Msg::Start),
            AppState::BuildCharacter(_) => build_character::handle(event).map(Msg::BuildCharacter),
            AppState::Main(_) => main::handle(event).map(Msg::Main),
        }
    }

    fn update(&mut self, msg: Msg, orders: &mut impl Orders<Msg>, ctx: &mut Ctx) -> Trans {
        match (self, msg) {
            (&mut AppState::Init, _) => Trans::Continue,
            (&mut AppState::Start(ref mut state), Msg::Start(msg)) => {
                start::update(msg, state, &mut orders.proxy(Msg::Start), ctx)
            }
            (&mut AppState::BuildCharacter(ref mut state), Msg::BuildCharacter(ref msg)) => {
                build_character::update(
                    msg.clone(),
                    state,
                    &mut orders.proxy(Msg::BuildCharacter),
                    ctx,
                )
            }
            (&mut AppState::Quit(_), _) => Trans::Continue,
            (&mut AppState::Main(ref mut state), Msg::Main(ref msg)) => {
                main::update(msg.clone(), state, &mut orders.proxy(Msg::Main), ctx)
            }
            (_, x) => panic!(format!("state received invalid message: {:?}", x)),
        }
    }
}

impl Default for Model {
    fn default() -> Model {
        Model {
            ws: None,
            state: vec![AppState::Init],
        }
    }
}

pub struct Ctx(Vec<msg::Client>);

impl Ctx {
    pub fn send(&mut self, msg: msg::Client) {
        self.0.push(msg);
    }
}

#[derive(Clone, Debug)]
pub enum Msg {
    Init,
    Ws(WsMsg),
    Start(start::Msg),
    BuildCharacter(build_character::Msg),
    Main(main::Msg),
    Quit(quit::Msg),
}

// For now server messages and connection events,
// Will eventially have stuff like key presses
pub enum Event {
    Connected,
    Disconnected,
    Server(msg::Server),
}

pub fn handle_init<O>(msg: msg::init::Server, model: &mut Model, orders: &mut O)
where
    O: Orders<Msg, AppMs = Msg>,
    <O as Orders<Msg>>::ElC: View<Msg>,
{
    use msg::init::Server::*;
    match msg {
        Start => {
            model.state.push(AppState::Start(start::Model::default()));
            orders.skip();
        }
        BuildCharacter => {
            model
                .state
                .push(AppState::BuildCharacter(build_character::Model::default()));
            orders.skip();
        }
        Main => {
            model.state.push(AppState::Main(main::Model::default()));
        }
        Done => {
            orders.render();
        }
    }
}

pub fn update<O>(msg: Msg, model: &mut Model, orders: &mut O)
where
    O: Orders<Msg, AppMs = Msg>,
    <O as Orders<Msg>>::ElC: View<Msg>,
{
    trace!("msg: {:#?}", msg);
    let mut msg = Some(msg);
    let mut trans = Trans::Continue;
    loop {
        let state = model.state.last_mut().unwrap();

        if msg.is_none() {
            break;
        }

        match msg.take().unwrap() {
            // init means create a websocket and wait on connection
            Msg::Init => {
                let app_clone = orders.clone_app();
                let url = seed::document()
                    .location()
                    .and_then(|x| x.host().ok())
                    .unwrap_or_else(|| "127.0.0.1:8080".to_string());
                model.ws = Some(Ws::new(&format!("ws://{}/ws", url), Msg::Ws, app_clone));
                break;
            }
            Msg::Ws(WsMsg::Recv(msg::Server::Init(x))) => {
                handle_init(x, model, orders);
            }
            Msg::Ws(x) => match x {
                WsMsg::Connected => {
                    debug!("called!");
                    if state.is_init() {
                        model.ws.as_mut().unwrap().send(msg::Client::Init);
                    } else {
                        msg = state.handle(Event::Connected);
                    }
                }
                WsMsg::Lost => {
                    model.ws = None;
                    if let Some(AppState::Quit(_)) = model.state.last() {
                        return;
                    }
                    model.state = vec![AppState::Init];
                    let app = orders.clone_app();
                    seed::set_timeout(Box::new(move || app.update(Msg::Init)), 1000);
                    return;
                }
                WsMsg::Recv(x) => {
                    msg = state.handle(Event::Server(x));
                }
                WsMsg::Error => {
                    msg = state.handle(Event::Disconnected);
                }
            },
            x => {
                let mut ctx = Ctx(Vec::new());
                trans = state.update(x, orders, &mut ctx);
                if let Some(ws) = model.ws.as_mut() {
                    for x in ctx.0 {
                        ws.send(x);
                    }
                }
                break;
            }
        }
    }
    match trans {
        Trans::Replace(x) => {
            *model.state.last_mut().unwrap() = x;
        }
        Trans::Push(x) => {
            model.state.push(x);
        }
        Trans::Pop => {
            model.state.pop();
        }
        Trans::Continue => {}
    }
}

pub fn view(model: &Model) -> impl View<Msg> {
    match model.state.last().unwrap() {
        AppState::Init => div![
            class! {"container mx-auto h-screen flex justify-center items-center"},
            div![
                class! {"w-5/6 shadow-xl rounded p-4 bg-bg inline-flex items-center"},
                h1![class! {"mr-2"}, "Waiting for connection"],
                div![class! {"spinner"}, html::icon("sync"),]
            ]
        ]
        .els(),
        AppState::Start(mdl) => start::view(mdl).els().map_msg(Msg::Start),
        AppState::Quit(mdl) => quit::view(mdl).els().map_msg(Msg::Quit),
        AppState::BuildCharacter(mdl) => build_character::view(mdl)
            .els()
            .map_msg(Msg::BuildCharacter),
        AppState::Main(mdl) => main::view(mdl).els().map_msg(Msg::Main),
    }
}
