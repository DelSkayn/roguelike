#![recursion_limit = "512"]
#![allow(dead_code)]

#[macro_use]
extern crate log;
#[macro_use]
extern crate seed;
#[macro_use]
extern crate serde_derive;

mod app;
mod html;
mod panic;
mod states;
mod ws;

use seed::prelude::{AfterMount, BeforeMount, Orders, Url};

fn after_mount(_url: Url, _orders: &mut impl Orders<app::Msg>) -> AfterMount<app::Model> {
    AfterMount::new(app::Model::default())
}

use wasm_bindgen::prelude::*;
// This is the entry point
#[wasm_bindgen(start)]
pub fn run() {
    panic::set_panic_hook();
    wasm_logger::init(wasm_logger::Config::new(log::Level::Trace).message_on_new_line());
    info!("starting frontend!");
    println!("{:?}", seed::browser::url::current());

    let app = seed::App::builder(app::update, app::view)
        .before_mount(|_| BeforeMount::new().mount_point(seed::body()))
        .after_mount(after_mount)
        .build_and_start();
    // Start the actual app.
    app.update(app::Msg::Init);
}
