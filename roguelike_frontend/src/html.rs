use seed::prelude::*;

/// A icon with a specific name from fontawesome
pub fn icon<M: 'static>(name: &str) -> Node<M> {
    let class_name = format!("fas fa-{}", name);
    span![
        class! {"subpixel-antialiased"},
        i![attrs! {"class" => class_name}]
    ]
}

/// A card from bulma
pub fn card<M, V>(title: &str, content: V) -> Node<M>
where
    M: 'static,
    V: View<M>,
{
    div![
        class! {"card"},
        div![
            class! {"card-header"},
            p![class! {"card-header-title"}, title]
        ],
        div![
            class! {"card-content"},
            div![class! {"content"}, content.els()]
        ]
    ]
}
