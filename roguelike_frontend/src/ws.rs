use bincode;
use js_sys::{ArrayBuffer, Uint8Array};
use roguelike_common::msg;
use seed::prelude::*;
use seed::App;
use serde::Serialize;
use wasm_bindgen::{JsCast, JsValue};
use web_sys::{MessageEvent, WebSocket};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum WsMsg {
    Connected,
    Lost,
    Error,
    Recv(msg::Server),
}

#[derive(Debug)]
pub struct Ws {
    _on_close: Closure<dyn FnMut(JsValue)>,
    _on_open: Closure<dyn FnMut(JsValue)>,
    _on_error: Closure<dyn FnMut(JsValue)>,
    _on_message: Closure<dyn FnMut(JsValue)>,
    ws: WebSocket,
}

impl Ws {
    pub fn new<F, Msg, Mdl, ElC, GMs>(url: &str, func: F, app: App<Msg, Mdl, ElC, GMs>) -> Self
    where
        ElC: View<Msg> + 'static,
        GMs: 'static,
        Msg: Clone + 'static,
        F: Fn(WsMsg) -> Msg + 'static + Copy,
    {
        let ws = WebSocket::new(url).unwrap();
        js_sys::Reflect::set(
            &ws,
            &JsValue::from("binaryType"),
            &JsValue::from("arraybuffer"),
        )
        .unwrap();

        let app_clone = app.clone();
        let on_close = Closure::new(move |_| {
            debug!("websocket close");
            app_clone.update(func(WsMsg::Lost));
        });
        ws.set_onclose(Some(on_close.as_ref().unchecked_ref()));

        let app_clone = app.clone();
        let on_error = Closure::new(move |_| {
            debug!("websocket error");
            app_clone.update(func(WsMsg::Error));
        });
        ws.set_onerror(Some(on_error.as_ref().unchecked_ref()));

        let app_clone = app.clone();
        let on_open = Closure::new(move |_| {
            debug!("websocket connect");
            app_clone.update(func(WsMsg::Connected));
        });
        ws.set_onopen(Some(on_open.as_ref().unchecked_ref()));

        let app_clone = app.clone();
        let on_message = Closure::new(move |e: JsValue| {
            let e = MessageEvent::from(e);
            let data = e.data();
            if data.is_instance_of::<ArrayBuffer>() {
                let buffer = Uint8Array::new(&data);
                let mut buf = vec![0; buffer.length() as usize];
                buffer.copy_to(&mut buf[0..]);
                let res = bincode::deserialize(&buf);
                if res.is_err() {
                    warn!("recieved invalid message");
                    return;
                }
                let res = res.unwrap();
                trace!("MESSAGE RECV: {:#?}", res);
                app_clone.update(func(WsMsg::Recv(res)));
            } else {
                warn!("ignoring non binary websocket data");
            }
        });
        ws.set_onmessage(Some(on_message.as_ref().unchecked_ref()));

        Ws {
            _on_close: on_close,
            _on_open: on_open,
            _on_error: on_error,
            _on_message: on_message,
            ws,
        }
    }

    pub fn send(&self, message: msg::Client) {
        trace!("MESSAGE SEND: {:#?}", message);
        let mut value = bincode::serialize(&message).unwrap();
        self.ws.send_with_u8_array(&mut value).unwrap();
    }
}

impl Drop for Ws {
    fn drop(&mut self) {
        self.ws.close().unwrap();
    }
}
