use crate::{app::prelude::*, states::build_character};
use roguelike_common::msg;

#[derive(Debug)]
pub struct Model;

impl Default for Model {
    fn default() -> Self {
        Model
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Msg {
    Start,
    Quit,
    TransNewGame,
}

pub fn handle(event: Event) -> Option<Msg> {
    match event {
        Event::Server(msg::Server::Start(msg::start::Server::NewGame)) => Some(Msg::TransNewGame),
        _ => None,
    }
}

pub fn update(msg: Msg, _model: &mut Model, _order: &mut impl Orders<Msg>, ctx: &mut Ctx) -> Trans {
    match msg {
        Msg::Start => ctx.send(msg::Client::Start(msg::start::Client::NewGame)),
        Msg::Quit => {
            ctx.send(msg::Client::Start(msg::start::Client::Quit));
            return Trans::Push(AppState::Quit(Default::default()));
        }
        Msg::TransNewGame => {
            return Trans::Push(AppState::BuildCharacter(build_character::Model::default()));
        }
    };
    Trans::Continue
}

pub fn view(_model: &Model) -> impl View<Msg> {
    div![
        class! {"container mx-auto h-screen flex justify-center items-center"},
        div![
            class! {"w-5/6 shadow-xl rounded p-4 bg-bg"},
            div![
                div![
                    class! {"mb-2"},
                    h1![class! {"font-mono"}, "ROGUELIKE"],
                    h3![class! {"font-thin"}, "Welcome!"],
                ],
                div![
                    class! {"flex flex-col md:flex-row"},
                    button![
                        class! {"btn rounded mb-2 md:mb-0 md:rounded-r-none"},
                        simple_ev(Ev::Click, Msg::Start),
                        span!["New Game"]
                    ],
                    button![
                        class! {"btn rounded mb-2 md:mb-0 md:rounded-none"},
                        span!["Load Game"]
                    ],
                    button![
                        class! {"btn rounded md:rounded-l-none"},
                        simple_ev(Ev::Click, Msg::Quit),
                        span!["Quit"]
                    ]
                ]
            ]
        ]
    ]
}
