use crate::{
    app::prelude::*,
    html::{card, icon},
    states::main,
};
use immers::{MapPatchable, Patchable};
use roguelike_common::msg::{
    self,
    build_character::{Character, Sex, Stat},
};

#[derive(Debug, Default)]
pub struct Model {
    character: Character,
}

#[derive(Debug, Clone)]
pub enum Msg {
    /// a stat should be increased.
    IncreaseStat(Stat),
    /// a stat should be decreased.
    DecreaseStat(Stat),
    /// the firstname should be altered.
    AlterFirstname(String),
    /// the surname should be altered.
    AlterSurname(String),
    /// the surname should be altered.
    AlterSex(Sex),
    /// the server changed value.
    Patch(<Character as Patchable>::Patch),
    /// Request to go back to the start screen
    Cancel,
    /// Go back to the start screen
    ConfirmCancel,
    /// Request to finish charcter building
    Finish,
    /// Server confirmed finishing character
    FinishConfirm,
}

pub fn handle(event: Event) -> Option<Msg> {
    use msg::build_character::Server as Ev;
    match event {
        Event::Server(msg::Server::BuildCharacter(x)) => match x {
            Ev::Patch(p) => Some(Msg::Patch(p)),
            Ev::Cancel => Some(Msg::ConfirmCancel),
            Ev::Confirm => Some(Msg::FinishConfirm), //TODO
        },
        _ => None,
    }
}

pub fn update(msg: Msg, model: &mut Model, order: &mut impl Orders<Msg>, ctx: &mut Ctx) -> Trans {
    let mut send = |x| ctx.send(msg::Client::BuildCharacter(x));
    use msg::build_character::Client::*;
    match msg {
        Msg::IncreaseStat(stat) => {
            let patch = model.character.map_produce(|mut c| {
                *c.stats.get_stat_mut(stat) += 1.0;
                c
            });
            send(Patch(patch.unwrap()));
            order.skip();
        }
        Msg::DecreaseStat(stat) => {
            let patch = model.character.map_produce(|mut c| {
                *c.stats.get_stat_mut(stat) -= 1.0;
                c
            });
            send(Patch(patch.unwrap()));
            order.skip();
        }
        Msg::AlterFirstname(name) => {
            let patch = model.character.map_produce(|mut c| {
                c.first_name = name;
                c
            });
            if let Some(p) = patch {
                send(Patch(p));
            }
            order.skip();
        }
        Msg::AlterSurname(name) => {
            let patch = model.character.map_produce(|mut c| {
                c.last_name = name;
                c
            });
            if let Some(p) = patch {
                send(Patch(p));
            }
            order.skip();
        }
        Msg::AlterSex(sex) => {
            let patch = model.character.map_produce(|mut c| {
                c.is_male = sex == Sex::Male;
                c
            });
            if let Some(p) = patch {
                send(Patch(p));
            }
            order.skip();
        }
        Msg::Patch(p) => {
            model
                .character
                .apply(p)
                .map_err(|e| error!("invalid patch! {}", e))
                .ok();
        }
        Msg::Cancel => {
            send(Cancel);
        }
        Msg::ConfirmCancel => {
            return Trans::Pop;
        }
        Msg::Finish => {
            send(Confirm);
        }
        Msg::FinishConfirm => return Trans::Replace(AppState::Main(main::Model::default())),
    };
    Trans::Continue
}

pub fn info_view(model: &Model) -> Node<Msg> {
    section![
        class! {"md:flex md:items-center w-full"},
        div![
            class! {"md:flex-1 p-1"},
            label![class! {"text-xl font-mono"}, "Firstname"],
            input![
                class! {"appearance-none border-accent border-2 px-4 py-2 rounded bg-transparent shadow-inner w-full focus:border-accent-alt"},
                attrs! {At::Type => "text"},
                attrs! {At::Placeholder => "Enter firstname"},
                attrs! {At::Value => model.character.first_name},
                input_ev(Ev::Input, Msg::AlterFirstname)
            ],
        ],
        div![
            class! {"md:flex-1 p-1"},
            label![class! {"text-xl font-mono"}, "Surname"],
            input![
                class! {"appearance-none border-accent border-2 px-4 py-2 rounded bg-transparent shadow-inner w-full focus:border-accent-alt"},
                attrs! {At::Type => "text"},
                attrs! {At::Placeholder => "Enter surname"},
                attrs! {At::Value => model.character.last_name},
                input_ev(Ev::Input, Msg::AlterSurname)
            ],
        ],
        div![
            class! {"md:flex-1 p-1 flex md:flex-col md:mt-8"},
            label![
                class! {"font-mono flex items-center"},
                input![
                    attrs! {At::Type => "radio"},
                    attrs! {At::Name => "sex"},
                    attrs! {At::Checked => (model.character.is_male).as_at_value()},
                    class! {"radio"},
                    simple_ev(Ev::Click, Msg::AlterSex(Sex::Male))
                ],
                span![class! {"ml-2"}, "Male"]
            ],
            label![
                class! {"font-mono flex items-center ml-4 md:ml-0"},
                input![
                    class! {""},
                    attrs! {At::Type => "radio"},
                    attrs! {At::Name => "sex"},
                    attrs! {At::Checked => (!model.character.is_male).as_at_value()},
                    class! {"radio"},
                    simple_ev(Ev::Click, Msg::AlterSex(Sex::Female))
                ],
                span![class! {"ml-2"}, "Female"]
            ]
        ],
        div![class! {"column"}]
    ]
}

pub fn stats_view(model: &Model) -> Node<Msg> {
    let stats = [
        (Stat::Strength, "Strength"),
        (Stat::Agility, "Agility"),
        (Stat::Intelligence, "Intelligence"),
        (Stat::Constitution, "Constitution"),
        (Stat::Wisdom, "Wisdom"),
        (Stat::Charisma, "Charisma"),
    ];

    let stats_buttons: Vec<_> = stats
        .iter()
        .map(|(stat, name)| {
            div![
                class!{"flex justify-between p-1"},
                label![class! {"text-xl font-mono"}, name],
                div![
                    class! {"inline-flex items-center content-center rounded-full shadow p-0 bg-fg"},
                    button![
                        class! {"btn active:shadow-outline rounded-full rounded-r-none flex h-8 w-8 items-center justify-center mr-2"},
                        simple_ev(Ev::Click, Msg::IncreaseStat(*stat)),
                        icon::<Msg>("plus").els()
                    ],
                    h3![
                        class! {"font-bold text-bg"},
                        format!("{}", model.character.stats.get_stat(*stat))
                    ],
                    button![
                        class! {"btn active:shadow-outline rounded-full rounded-l-none flex h-8 w-8 items-center justify-center ml-2"},
                        simple_ev(Ev::Click, Msg::DecreaseStat(*stat)),
                        icon::<Msg>("minus").els()
                    ],
                ],
            ]
        })
        .collect();
    div![
        h1![class! {"text-3xl"}, "Stats"],
        hr![class! {"mb-2"}],
        div![class! {""}, stats_buttons]
    ]
}

pub fn view(model: &Model) -> impl View<Msg> {
    div![
        class! {"container mx-auto shadow-lg bg-bg p-4"},
        section![
            class! {"section"},
            h1![class! {"text-3xl md:text-6xl"}, "Create a new character!"],
        ],
        info_view(model),
        section![
            class! {"section"},
            div![
                class! {""},
                div![class! {""}, stats_view(model),],
                div![class! {""}, card("something else", div![])]
            ]
        ],
        section![
            button![
                class! {"btn btn-red mr-2"},
                simple_ev(Ev::Click, Msg::Cancel),
                "Cancel"
            ],
            button![
                class! {"btn btn-green"},
                simple_ev(Ev::Click, Msg::Finish),
                "Finish"
            ],
        ]
    ]
}
