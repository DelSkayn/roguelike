use seed::prelude::*;

#[derive(Debug)]
pub struct Model;

impl Default for Model {
    fn default() -> Self {
        Model
    }
}

#[derive(Debug, Clone)]
pub enum Msg {}

pub fn view(_model: &Model) -> impl View<Msg> {
    div![
        class! {"container mx-auto h-screen flex justify-center items-center"},
        div![
            class! {"w-5/6 shadow-xl rounded p-4 bg-bg"},
            div![
                h1![class! {"text-3xl"}, "Roguelike stopped"],
                p![class! {"text-xl"}, "Have a nice day!"],
                p![
                    class! {"font-thin text-gray"},
                    "You can close this page now."
                ]
            ]
        ]
    ]
}
