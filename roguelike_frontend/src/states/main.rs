use crate::{app::prelude::*, html};
use immers::Patchable;
use roguelike_common::msg::{
    self,
    main::{Client, PlayerInfo, Server, StoryState},
};

#[derive(Debug)]
enum Modal {
    ConfirmExit,
    None,
}

impl Default for Modal {
    fn default() -> Modal {
        Modal::None
    }
}

#[derive(Default, Debug)]
pub struct Model {
    player_info: PlayerInfo,
    menu_shown: bool,
    modal: Modal,
    story_state: StoryState,
}

#[derive(Clone, Debug)]
pub enum Msg {
    ToggleMenu,
    /// The player pressed the exit button
    PressedExit,
    /// We confirmed that the player wants to exit,
    ConfirmedExit,
    /// We recieved confirmation from the server that we can exit
    RecievedExit,
    /// Remove the modal message
    CancelModal,
    /// A change to the player info.
    PlayerInfo(<PlayerInfo as Patchable>::Patch),
    StateChange(StoryState),
    TakeOption(usize),
}

pub fn handle(event: Event) -> Option<Msg> {
    match event {
        Event::Server(msg::Server::Main(x)) => match x {
            Server::Exit => Some(Msg::RecievedExit),
            Server::PlayerInfo(p) => Some(Msg::PlayerInfo(p)),
            Server::StateChange(x) => Some(Msg::StateChange(x)),
        },
        _ => None,
    }
}

pub fn update(msg: Msg, model: &mut Model, order: &mut impl Orders<Msg>, ctx: &mut Ctx) -> Trans {
    let mut send = |x| ctx.send(msg::Client::Main(x));
    match msg {
        Msg::ToggleMenu => {
            model.menu_shown = !model.menu_shown;
        }
        Msg::PressedExit => {
            model.modal = Modal::ConfirmExit;
        }
        Msg::ConfirmedExit => {
            send(Client::Exit);
            order.skip();
        }
        Msg::RecievedExit => {
            return Trans::Pop;
        }
        Msg::CancelModal => {
            model.modal = Modal::None;
        }
        Msg::PlayerInfo(x) => {
            model
                .player_info
                .apply(x)
                .map_err(|e| {
                    warn!("invalid PlayerInfo patch: {}", e);
                })
                .ok();
        }
        Msg::StateChange(x) => {
            model.story_state = x;
        }
        Msg::TakeOption(x) => {
            send(Client::TakeOption(x));
        }
    }
    Trans::Continue
}

pub fn view(model: &Model) -> impl View<Msg> {
    let head = header![
        class! {"z-10 fixed h-16 w-full bg-bg-darkest shadow flex justify-between items-center"},
        div![
            class! {
             "mx-4 text-accent hover:text-accent-alt select-none cursor-pointer"
            },
            h1![class! {"font-mono text-4xl"}, "RL"],
        ],
        div![
            class! {
                "mx-4 select-none text-3xl cursor-pointer lg:hidden"
            },
            html::icon("bars"),
            simple_ev(Ev::Click, Msg::ToggleMenu),
        ]
    ];

    let side_bar = div![
        class! {
            "fixed h-screen pt-16 bg-bg-darker w-0 shadow-xl overflow-x-hidden transition select-none max-w-sm"
            "lg:w-screen lg:relative",
            "w-screen" => model.menu_shown,
        },
        div![
            class! {"p-4"},
            div![
                class! {
                    "p-2 shadow rounded mb-2"
                },
                h1![
                    class! {"font-thin"},
                    format!(
                        "{} {}",
                        model.player_info.first_name, model.player_info.last_name
                    )
                ]
            ],
            div![button![
                class! {"btn btn-red"},
                simple_ev(Ev::Click, Msg::PressedExit),
                "Exit"
            ],]
        ]
    ];

    let options: Vec<_> = model
        .story_state
        .options
        .iter()
        .enumerate()
        .map(|(idx, name)| {
            button![
                class! {"mr-2 btn btn-read"},
                simple_ev(Ev::Click, Msg::TakeOption(idx)),
                name
            ]
        })
        .collect();
    let content = div![
        class! {"flex-1 pt-16"},
        div![
            class! {"p-4"},
            div![
                class! {"bg-bg shadow-xl"},
                Node::from_markdown(&model.story_state.text),
                div![class! {"mt-4"}, options]
            ]
        ]
    ];

    let modal = match model.modal {
        Modal::None => div![],
        Modal::ConfirmExit => div![
            class! {"z-50 fixed w-screen h-screen inset-0 flex items-center justify-center"},
            div![
                class! {"relative p-2 inset-auto bg-bg shadow-xl rounded w-5/6 text-center max-w-md"},
                p!["Are you sure you want to quit. All unsaved progress will be lost!"],
                div![
                    button![
                        class! {"btn btn-green mr-2"},
                        simple_ev(Ev::Click, Msg::CancelModal),
                        "Continue"
                    ],
                    button![
                        class! {"btn btn-red"},
                        simple_ev(Ev::Click, Msg::ConfirmedExit),
                        "Quit"
                    ],
                ]
            ]
        ],
    };

    div![
        head,
        div![class! {"w-screen flex"}, side_bar, content],
        modal
    ]
}
