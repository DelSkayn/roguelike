pub enum Msg {
    Disconnected,
    Connected,
}

pub struct Start {
    main_link: ComponentLink<Main>,
}

pub struct StartNav;

pub struct StartMain {}

impl Component for Main {
    type Message = Msg;
    type Properties = ();
}

impl Renderable<Start> for Start {
    fn view(&self) -> Html<Self> {
        html! {
            <div>
                <nav class="navbar is-fixed-top">
                    <div class="navbar-brand">
                        <h2>Roguelike</h2>
                    </div>
                </nav>
                <section class="hero is-fullheight-with-navbar">
                    <div class="hero-body">
                        <div class="columns">
                            <div class="column is-half is-offset-one-quarter">
                                <div class="box">
                                    <h1 class="title is-5">{ "ROGUELIKE" }</h1>
                                    <p class="subtitle">{"Welcome"}</p>
                                    <button class="button"
                                        onclick=|_| Msg::Start
                                        disabled=!self.connected
                                        >{ "Start" }</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        }
    }
}
