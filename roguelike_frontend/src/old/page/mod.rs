use roguelike_common::ServerMessage;

pub struct PageMsg{
    Connected,
    Disconnected,
    ServerMessage(ServerMessage),
}

pub enum PageCmd{
    Render,
    Pop,
    Push(Box<dyn Page>),
    Replace(Box<dyn Page>),
    ClientMessage(ClientMessage),
    None,
}

pub trait Page{
    type Msg;

}


pub struct Pages{
    state: Vec<Box<dyn Page>>,
}

impl Component for Pages{
    type Message = Msg;
    type Properties = ();
}

impl Renderable<Self> for Pages{
    fn view(&self) -> Html<Pages>{
        let page = self.page.first().unwrap();
        if let Some(x) = page.nav(){
            html!{
                <div>
                    <nav class="navbar is-fixed-top">
                        x
                    </nav>
                    self.page.body()
                </div>
            }
        }else{
            html!{
                <div>
                    self.page.body()
                </div>
            }
        }
    }
}
