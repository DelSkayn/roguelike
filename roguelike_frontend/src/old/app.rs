use bincode;
use failure::Error;
use roguelike_common::{ClientMessage, ServerMessage};
use std::time::Duration;
use yew::{
    format::{Binary, Text},
    html,
    services::{
        timeout::{TimeoutService, TimeoutTask},
        websocket::{WebSocketService, WebSocketStatus, WebSocketTask},
    },
    Component, ComponentLink, Html, Renderable, ShouldRender,
};

pub struct App {
    link: ComponentLink<App>,
    ws_service: WebSocketService,
    timeout_service: TimeoutService,
    ws: Option<WebSocketTask>,
    timeout: Option<TimeoutTask>,
    connected: bool,
}

#[derive(Debug)]
pub enum Format {
    Text(Text),
    Binary(Binary),
}

impl From<Text> for Format {
    fn from(t: Text) -> Self {
        Format::Text(t)
    }
}

impl From<Binary> for Format {
    fn from(t: Binary) -> Self {
        Format::Binary(t)
    }
}

#[derive(Debug)]
pub enum WsAction {
    Lost,
    Opened,
    Data(Format),
}

#[derive(Debug)]
pub enum Msg {
    Init,
    Start,
    WsAction(WsAction),
    Ignore,
    Send(ClientMessage),
    Recieved(ServerMessage),
}

impl Component for App {
    // Some details omitted. Explore the examples to see more.

    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, mut link: ComponentLink<Self>) -> Self {
        link.send_self(Msg::Init);
        App {
            link,
            ws_service: WebSocketService::new(),
            timeout_service: TimeoutService::new(),
            ws: None,
            timeout: None,
            connected: false,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        trace!("event: {:#?}", msg);
        match msg {
            Msg::Init => {
                self.init();
                true
            }
            Msg::WsAction(x) => self.handle_websocket(x),
            Msg::Start => {
                if self.ws.is_some() {
                    self.link.send_self(Msg::Send(ClientMessage::Init));
                }
                false
            }
            Msg::Ignore => false,
            Msg::Send(x) => {
                if let Some(ws) = self.ws.as_mut() {
                    ws.send_binary(bincode::serialize(&x).map_err(Error::from));
                } else {
                    error!("tried to send message but socket was not connected")
                }
                false
            }
            Msg::Recieved(_) => false,
        }
    }
}

impl App {
    fn init(&mut self) {
        let callback = self
            .link
            .send_back(|f: Format| Msg::WsAction(WsAction::Data(f)));
        let notification = self.link.send_back(|status| match status {
            WebSocketStatus::Error | WebSocketStatus::Closed => Msg::WsAction(WsAction::Lost),
            WebSocketStatus::Opened => Msg::WsAction(WsAction::Opened),
        });
        let ws = self
            .ws_service
            .connect("ws://127.0.0.1:8000/ws", callback, notification);
        self.ws = Some(ws);
    }

    fn handle_websocket(&mut self, action: WsAction) -> bool {
        match action {
            WsAction::Lost => {
                self.ws = None;
                let callback = self.link.send_back(|_| Msg::Init);
                self.timeout = Some(self.timeout_service.spawn(Duration::from_secs(1), callback));
                self.connected = false;
                true
            }
            WsAction::Opened => {
                self.timeout = None;
                self.link.send_self(Msg::Send(ClientMessage::Init));
                self.connected = true;
                true
            }
            WsAction::Data(f) =>
            /* TODO */
            {
                let data = match f {
                    Format::Binary(x) => x,
                    Format::Text(x) => {
                        warn!("recieved unsupported text message: {:?}", x);
                        return false;
                    }
                };

                let message = match data {
                    Ok(data) => bincode::deserialize(&data).map_err(|x| {
                        warn!("could not deserialize message: {}", x);
                        ()
                    }),
                    Err(e) => {
                        warn!("error reading binary data: {}", e);
                        return false;
                    }
                };
                if let Ok(x) = message {
                    self.link.send_self(Msg::Recieved(x));
                }
                false
            }
        }
    }
}

impl Renderable<App> for App {
    fn view(&self) -> Html<Self> {
        html! {
            <div class="columns">
                <div class="column is-half is-offset-one-quarter">
                    <div class="box">
                        <h1 class="title is-5">{ "ROGUELIKE" }</h1>
                        <p class="subtitle">{"Welcome"}</p>
                        <button class="button"
                            onclick=|_| Msg::Start
                            disabled=!self.connected
                            >{ "Start" }</button>
                    </div>
                </div>
            </div>
        }
    }
}
